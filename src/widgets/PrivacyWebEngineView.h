/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022-2025 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PRIVACY_WEBENGINE_VIEW_H
#define PRIVACY_WEBENGINE_VIEW_H

// Application headers.
#include "structs/RequestStruct.h"

// KDE framework headers.
#include <KLineEdit>

// Qt framework headers.
#include <QIcon>
#include <QList>
#include <QNetworkCookie>
#include <QWebEngineFindTextResult>
#include <QWebEngineView>

class PrivacyWebEngineView : public QWebEngineView
{
    // Include the Q_OBJECT macro.
    Q_OBJECT

public:
    // The default constructor.
    explicit PrivacyWebEngineView(QWidget *parentWidgetPointer = nullptr);

    // The public static constants.
    static const int ULTRAPRIVACY = 0;
    static const int ULTRALIST = 1;
    static const int EASYPRIVACY = 2;
    static const int EASYLIST = 3;
    static const int FANBOYS_ANNOYANCE_LIST = 4;
    static const int TOTAL = 5;

    // The public variables.
    QVector<int> blockedRequestsVector = {0, 0, 0, 0, 0, 0};
    std::list<QNetworkCookie> *cookieListPointer = new std::list<QNetworkCookie>;
    QString currentHost = QLatin1String("");
    QString currentUrlText = QLatin1String("");
    double defaultZoomFactor = 1.00;
    QString domainSettingsName = QLatin1String("");
    bool easyListEnabled = true;
    bool easyPrivacyEnabled = true;
    bool fanboysAnnoyanceListEnabled = true;
    QIcon favoriteIcon = QIcon::fromTheme(QLatin1String("globe"), QIcon::fromTheme(QLatin1String("applications-internet")));
    bool findCaseSensitive = false;
    QString findString = QLatin1String("");
    QWebEngineFindTextResult findTextResult = QWebEngineFindTextResult();
    int httpAuthenticationDialogsDisplayed = 0;
    bool isLoading = false;
    int loadProgressInt = -1;
    bool localStorageEnabled = false;
    QList<RequestStruct *> *requestsListPointer = new QList<RequestStruct *>;
    bool ultraListEnabled = true;
    bool ultraPrivacyEnabled = true;

Q_SIGNALS:
    // The signals.
    void numberOfCookiesChanged(const int numberOfCookies) const;
    void displayHttpPingBlockedDialog(const QString &httpPingUrl) const;
    void requestBlocked(const QVector<int> blockedRequestsVector) const;
    void updateUi(const PrivacyWebEngineView *privacyWebEngineViewPointer) const;

public Q_SLOTS:
    // The public slots.
    void addCookieToList(const QNetworkCookie &cookie) const;
    void applyDomainSettings(const QUrl &newUrl, const bool navigatingHistory);
    void removeCookieFromList(const QNetworkCookie &cookie) const;

private Q_SLOTS:
    // The private slots.
    void clearRequestsList();
    void displayHttpPingDialog(const QString &httpPingUrl) const;
    void handleAuthenticationRequest(const QUrl &requestUrl, QAuthenticator *authenticatorPointer);
    void manualOpenInNewTab();
    void openWithChromium() const;
    void openWithFirefox() const;
    void saveHoveredLink(const QString &hoveredLink);
    void storeRequest(RequestStruct *requestStructPointer);
    void storeUpdatedUrl(const QUrl &newUrl);

private:
    // The private variables.
    QString hoveredLinkString;
    bool manuallyOpeninginNewTab = false;
    KLineEdit *passwordLineEditPointer;
    KLineEdit *usernameLineEditPointer;
    QWebEngineProfile *webEngineProfilePointer;
    QWebEngineSettings *webEngineSettingsPointer;

protected:
    // The protected functions.
    void contextMenuEvent(QContextMenuEvent *contextMenuEvent) override;
    QWebEngineView* createWindow(QWebEnginePage::WebWindowType webWindowType) override;
};
#endif
