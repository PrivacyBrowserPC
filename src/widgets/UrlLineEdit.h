/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2024 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef URLLINEEDIT_H
#define URLLINEEDIT_H

// KDE Framework headers.
#include <KLineEdit>

class UrlLineEdit : public KLineEdit
{
    // Include the Q_OBJECT macro.
    Q_OBJECT

public:
    // The default constructor.
    explicit UrlLineEdit(QWidget *parentWidgetPointer = nullptr);

    // The public functions.
    void setText(const QString &urlString) override;

protected:
    // The protected functions.
    void focusInEvent(QFocusEvent *focusEventPointer) override;
    void focusOutEvent(QFocusEvent *focusEventPointer) override;
};
#endif
