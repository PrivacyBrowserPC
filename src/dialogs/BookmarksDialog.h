/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2023-2024 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BOOKMARKSDIALOG_H
#define BOOKMARKSDIALOG_H

// Application headers.
#include "structs/BookmarkStruct.h"
#include "widgets/DraggableTreeView.h"

// Qt toolkit headers.
#include <QDialog>
#include <QItemSelectionModel>
#include <QStandardItem>

class BookmarksDialog : public QDialog
{
    // Include the Q_OBJECT macro.
    Q_OBJECT

public:
    // The primary constructor.
    BookmarksDialog(QWidget *parentWidgetPointer, QIcon currentWebsiteFavorieIcon, QString currentWebsiteTitle, QString currentWebsiteUrl);

    // The public constants.
    static const int NAME_COLUMN = 0;
    static const int URL_COLUMN = 1;
    static const int DATABASE_ID_COLUMN = 2;
    static const int DISPLAY_ORDER_COLUMN = 3;
    static const int IS_FOLDER_COLUMN = 4;
    static const int FOLDER_ID_COLUMN = 5;

Q_SIGNALS:
    // The signals.
    void bookmarkUpdated() const;

private Q_SLOTS:
    // The private slots.
    void deleteItems() const;
    void refreshBookmarks() const;
    void showAddBookmarkDialog();
    void showAddFolderDialog();
    void showEditDialog();
    void updateBookmarkFromTree(QStandardItem *modifiedStandardItem);
    void updateSelection() const;

private:
    // The private variables.
    QPushButton *deleteItemsButtonPointer;
    QPushButton *editButtonPointer;
    QStandardItemModel *treeModelPointer;
    QItemSelectionModel *treeSelectionModelPointer;
    DraggableTreeView *draggableTreeViewPointer;
    QIcon websiteFavoriteIcon;
    QString websiteTitle;
    QString websiteUrl;

    // The private functions.
    void populateBookmarks() const;
    void populateSubfolders(QStandardItem *folderItemNamePointer, const double folderId) const;
    void selectSubfolderContents(const QModelIndex &parentModelIndex) const;
    void updateUi() const;
};
#endif
