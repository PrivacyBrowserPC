/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2024 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

// KDE Framework headers.
#include <KConfigDialog>

// Qt toolkit headers.
#include <QComboBox>
#include <QLabel>

class SettingsDialog : public KConfigDialog
{
    // Include the Q_OBJECT macro.
    Q_OBJECT

public:
    // The primary constructor.
    explicit SettingsDialog(QWidget *parentWidgetPointer, KCoreConfigSkeleton *coreConfigSkeletonPointer);

Q_SIGNALS:
    void spellCheckLanguagesUpdated() const;

private Q_SLOTS:
    void showDownloadDirectoryBrowseDialog();
    void updateSearchEngineLabel(const QString &searchEngineString) const;
    void updateUserAgentLabel(const QString &userAgentDatabaseName) const;

private:
    // The private variables.
    QComboBox *downloadDirectoryComboBoxPointer;
    QLabel *searchEngineLabelPointer;
    QLabel *userAgentLabelPointer;
};
#endif
