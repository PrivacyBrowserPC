/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022-2024 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Application headers.
#include "SaveDialog.h"
#include "ui_SaveDialog.h"

// KDE Frameworks headers.
#include <KLocalizedString>

// Qt toolkit headers.
#include <QMimeDatabase>
#include <QPushButton>
#include <QShortcut>
#include <QStandardPaths>

SaveDialog::SaveDialog(QWidget *parentWidgetPointer, QUrl &url, QString &mimeTypeString, int totalBytes, QString fileName, bool nativeDownloader) :
                       QDialog(parentWidgetPointer), downloadUrl(url), suggestedFileName(fileName)
{
    // Set the dialog window title.
    setWindowTitle(i18nc("The save dialog window title", "Save"));

    // Set the window modality.
    setWindowModality(Qt::WindowModality::ApplicationModal);

    // Instantiate the save dialog UI.
    Ui::SaveDialog saveDialogUi;

    // Setup the UI.
    saveDialogUi.setupUi(this);

    // Get handles for the widgets.
    QGraphicsView *mimeGraphicsViewPointer = saveDialogUi.mimeGraphicsView;
    QLabel *urlLabelPointer = saveDialogUi.urlLabel;
    QLabel *filetypeLabelPointer = saveDialogUi.fileTypeLabel;
    QLabel *mimeTypeLabelPointer = saveDialogUi.mimeTypeLabel;
    QLabel *sizeLabelPointer = saveDialogUi.sizeLabel;
    QDialogButtonBox *dialogButtonBoxPointer = saveDialogUi.dialogButtonBox;
    QPushButton *saveButtonPointer = dialogButtonBoxPointer->button(QDialogButtonBox::Save);

    // Get a MIME type database.
    QMimeDatabase mimeDatabase;

    // Get the MIME type.
    QMimeType mimeType = mimeDatabase.mimeTypeForName(mimeTypeString);

    // Get the MIME type icon.
    QIcon mimeTypeIcon = QIcon::fromTheme(mimeType.iconName());

    // Create a graphics scene.
    QGraphicsScene *mimeGraphicsScenePointer = new QGraphicsScene(this);

    // Set the graphics scene.
    mimeGraphicsViewPointer->setScene(mimeGraphicsScenePointer);

    // Set the background of the graphics view to be the same as the window
    mimeGraphicsViewPointer->setBackgroundRole(QPalette::Window);

    // Add the MIME type icon to the scene.
    mimeGraphicsScenePointer->addPixmap(mimeTypeIcon.pixmap(64, 64));

    // Populate the labels.
    urlLabelPointer->setText(QLatin1String("<b>") + downloadUrl.toString() + QLatin1String("</b>"));
    filetypeLabelPointer->setText(QLatin1String("<b>") + mimeType.comment() + QLatin1String("</b>"));
    mimeTypeLabelPointer->setText(QLatin1String("<b>") + mimeTypeString + QLatin1String("</b>"));

    // Populate the download size label.
    if (totalBytes == -1)  // The file size is unknown.
        sizeLabelPointer->setText(i18nc("Unknown download file size.  The bold style should be preserved.", "<b>unknown</b>"));
    else  // The file size is known.  Format it according to the locale.
        sizeLabelPointer->setText(ki18nc("Download file size.  The bold style should be preserved.", "<b>%1 bytes</b>").subs(totalBytes).toString());

    // Create the keyboard shortcuts.
    QShortcut *sShortcutPointer = new QShortcut(QKeySequence(i18nc("The save key shortcut.", "s")), this);
    QShortcut *cShortcutPointer = new QShortcut(QKeySequence(i18nc("The close key shortcut.", "c")), this);
    QShortcut *quitShortcutPointer = new QShortcut(QKeySequence::Quit, this);

    // Connect the save buttons.
    if (nativeDownloader)
    {
        // Show the file picker for the native download.
        connect(saveButtonPointer, SIGNAL(clicked()), this, SLOT(showFilePicker()));
        connect(sShortcutPointer, SIGNAL(activated()), this, SLOT(showFilePicker()));
    }
    else
    {
        // Use WebEngine's downloader.
        connect(saveButtonPointer, SIGNAL(clicked()), this, SLOT(accept()));
        connect(sShortcutPointer, SIGNAL(activated()), this, SLOT(accept()));
    }

    // Connect the cancel button.
    connect(dialogButtonBoxPointer, SIGNAL(rejected()), this, SLOT(reject()));
    connect(cShortcutPointer, SIGNAL(activated()), this, SLOT(reject()));
    connect(quitShortcutPointer, SIGNAL(activated()), this, SLOT(reject()));
}

void SaveDialog::showFilePicker()
{
    // Show the file picker dialog.
    Q_EMIT useNativeKdeDownloader(downloadUrl, suggestedFileName);

    // Close the dialog.
    reject();
}
