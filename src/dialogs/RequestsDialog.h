/*
 * Copyright 2024 Soren Stoutner <soren@stoutner.com>.
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * Privacy Browser PC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Privacy Browser PC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Privacy Browser PC.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REQUESTSDIALOG_H
#define REQUESTSDIALOG_H

// Application headers.
#include "structs/RequestStruct.h"

// Qt toolkit headers.
#include <QDialog>
#include <QTableWidget>

class RequestsDialog : public QDialog
{
    // Include the Q_OBJECT macro.
    Q_OBJECT

public:
    // The primary constructor.
    explicit RequestsDialog(QWidget *parentWidgetPointer, QList<RequestStruct *> *requestsListPointer);

private Q_SLOTS:
    void showRequestDetailDialog(int row);
    void sortTable(int column, Qt::SortOrder sortOrder) const;

private:
    // The private variables.
    QTableWidget *tableWidgetPointer;
};
#endif
