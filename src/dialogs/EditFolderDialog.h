/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2023-2024 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EDITFOLDERDIALOG_H
#define EDITFOLDERDIALOG_H

// Application headers.
#include "helpers/FolderHelper.h"
#include "structs/BookmarkStruct.h"

// Qt toolkit headers.
#include <QDialog>
#include <QRadioButton>

class EditFolderDialog : public QDialog
{
    // Include the Q_OBJECT macro.
    Q_OBJECT

public:
    // The primary constructor.
    explicit EditFolderDialog(QWidget *parentWidgetPointer, const int databaseId, QIcon &currentWebsiteFavoriteIcon);

Q_SIGNALS:
    // The signals.
    void folderSaved() const;

private Q_SLOTS:
    // The private slots.
    void browse();
    void save();
    void updateUi();

private:
    // The private variables.
    FolderHelper *folderHelperPointer;

    // The private widgets.
    QRadioButton *currentFolderIconRadioButtonPointer;
    QRadioButton *currentWebsiteFavoriteIconRadioButtonPointer;
    QRadioButton *customFolderIconRadioButtonPointer;
    QRadioButton *defaultFolderIconRadioButtonPointer;
    BookmarkStruct *folderBookmarkStructPointer;
    int folderDatabaseId;
    QLineEdit *folderNameLineEditPointer;
    QTreeWidget *parentFolderTreeWidgetPointer;
    QPushButton *saveButtonPointer;
};
#endif
