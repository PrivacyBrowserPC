 /*
 * Copyright 2024 Soren Stoutner <soren@stoutner.com>.
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * Privacy Browser PC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Privacy Browser PC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Privacy Browser PC.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REQUEST_DETAIL_DIALOG_H
#define REQUEST_DETAIL_DIALOG_H

// Application headers.
#include "structs/RequestStruct.h"

// Qt toolkit headers.
#include <QCheckBox>
#include <QDialog>
#include <QLabel>
#include <QTableWidget>

class RequestDetailDialog : public QDialog
{
    // Include the Q_OBJECT macro.
    Q_OBJECT

public:
    // The primary constructor.
    explicit RequestDetailDialog(QWidget *parentWidgetPointer, QTableWidget *tableWidgetPointer, const int initialRow);

private Q_SLOTS:
    // The private slots.
    void next();
    void previous();

private:
    // The private variables.
    QLineEdit *appliedEntryListLineEditPointer;
    QLineEdit *appliedFilterOptionsLineEditPointer;
    int currentRow;
    QLineEdit *dispositionLineEditPointer;
    QLineEdit *domainLineEditPointer;
    QLineEdit *domainListLineEditPointer;
    QWidget *filterListEntryWidget;
    QLineEdit *filterListLineEditPointer;
    QLineEdit *finalMatchLineEditPointer;
    QLineEdit *fontLineEditPointer;
    QCheckBox *hasRequestOptionsCheckBoxPointer;
    QLineEdit *imageLineEditPointer;
    QLineEdit *initialMatchLineEditPointer;
    bool isAllowList;
    QLineEdit *mainFrameLineEditPointer;
    QLineEdit *mediaLineEditPointer;
    QLineEdit *navigationTypeLineEditPointer;
    QPalette negativeBackgroundPalette;
    QPushButton *nextButtonPointer;
    QPalette normalBackgroundPalette;
    QLineEdit *objectLineEditPointer;
    QLineEdit *originalEntryLineEditPointer;
    QLineEdit *originalFilterOptionsLineEditPointer;
    QLineEdit *otherLineEditPointer;
    QLineEdit *pingLineEditPointer;
    QPalette positiveBackgroundPalette;
    QPushButton *previousButtonPointer;
    QLineEdit *requestMethodLineEditPointer;
    QLineEdit *requestUrlLineEditPointer;
    QLineEdit *requestUrlWithSeparatorsLineEditPointer;
    QLineEdit *resourceTypeLineEditPointer;
    QLineEdit *scriptLineEditPointer;
    QLineEdit *styleSheetLineEditPointer;
    QLineEdit *subFrameLineEditPointer;
    QLineEdit *sublistLineEditPointer;
    QTableWidget *tableWidgetPointer;
    QLineEdit *truncatedRequestUrlLineEditPointer;
    QLineEdit *truncatedRequestUrlWithSeparatorsLineEditPointer;
    QLineEdit *thirdPartyFilterLineEditPointer;
    QLineEdit *thirdPartyRequestLineEditPointer;
    QLineEdit *webPageUrlLineEditPointer;
    QLineEdit *xmlHttpRequestLineEditPointer;

    // The private functions.
    void populateDialog(const int row);
    void setFilterOptionBackgroundPalette(QLineEdit *lineEditPointer);
    void setInitialOrFinalBackgroundPalette(QLineEdit *lineEditPointer);
    void setRequestUrlBackgroundPalettes(RequestUrlType matchedUrlType);
};
#endif
