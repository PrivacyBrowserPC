/*
 * Copyright 2024 Soren Stoutner <soren@stoutner.com>.
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * Privacy Browser PC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Privacy Browser PC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Privacy Browser PC.  If not, see <http://www.gnu.org/licenses/>.
 */

// Application headers.
#include "RequestsDialog.h"
#include "GlobalVariables.h"
#include "RequestDetailDialog.h"
#include "ui_RequestsDialog.h"

// Qt toolkit headers.
#include <QDebug>

// KDE Frameworks headers.
#include <KColorScheme>

// Construct the class.
RequestsDialog::RequestsDialog(QWidget *parentWidgetPointer, QList<RequestStruct *> *requestsListPointer) : QDialog(parentWidgetPointer)
{
    // Set the window title.
    setWindowTitle(i18nc("The requests dialog window title", "Requests"));

    // Instantiate the requests dialog UI.
    Ui::RequestsDialog requestsDialogUi;

    // Setup the UI.
    requestsDialogUi.setupUi(this);

    // Get handles for the views.
    tableWidgetPointer = requestsDialogUi.tableWidget;
    QDialogButtonBox *dialogButtonBoxPointer = requestsDialogUi.dialogButtonBox;

    // Create the columns.
    tableWidgetPointer->setColumnCount(5);

    // Create the table headers.
    QTableWidgetItem *dispositionHeaderItemPointer = new QTableWidgetItem(i18nc("Request disposition header", "Disposition"));
    QTableWidgetItem *requestMethodHeaderItemPointer = new QTableWidgetItem(i18nc("Request method header", "Request Method"));
    QTableWidgetItem *navigationTypeHeaderItemPointer = new QTableWidgetItem(i18nc("Navigation type header", "Navigation Type"));
    QTableWidgetItem *resourceTypeHeaderItemPointer = new QTableWidgetItem(i18nc("Resource type header", "Resource Type"));
    QTableWidgetItem *requestUrlHeaderItemPointer = new QTableWidgetItem(i18nc("Request URL header", "Request URL"));

    // Set the horizontal headers.
    tableWidgetPointer->setHorizontalHeaderItem(0, dispositionHeaderItemPointer);
    tableWidgetPointer->setHorizontalHeaderItem(1, requestMethodHeaderItemPointer);
    tableWidgetPointer->setHorizontalHeaderItem(2, navigationTypeHeaderItemPointer);
    tableWidgetPointer->setHorizontalHeaderItem(3, resourceTypeHeaderItemPointer);
    tableWidgetPointer->setHorizontalHeaderItem(4, requestUrlHeaderItemPointer);

    // Create the palette.
    QPalette negativePalette = QPalette();
    QPalette positivePalette = QPalette();

    // Adjust the background colors of the palettes.
    KColorScheme::adjustBackground(negativePalette, KColorScheme::NegativeBackground);
    KColorScheme::adjustBackground(positivePalette, KColorScheme::PositiveBackground);

    // Prepare the background brushes.
    QBrush negativeBackgroundBrush = negativePalette.base();
    QBrush positiveBackgroundBrush = positivePalette.base();

    // Initialize the row counter.
    int rowCounter = 0;

    for (RequestStruct *requestStructPointer : *requestsListPointer)
    {
        // Add a new row.
        tableWidgetPointer->insertRow(rowCounter);

        // Create the entry items.
        QTableWidgetItem *dispositionItemPointer = new QTableWidgetItem(globalFilterListHelperPointer->getDispositionString(requestStructPointer->dispositionInt));
        QTableWidgetItem *requestMethodItemPointer = new QTableWidgetItem(requestStructPointer->requestMethodString);
        QTableWidgetItem *navigationTypeItemPointer = new QTableWidgetItem(globalFilterListHelperPointer->getNavigationTypeString(requestStructPointer->navigationTypeInt));
        QTableWidgetItem *resourceTypeItemPointer = new QTableWidgetItem(globalFilterListHelperPointer->getResourceTypeString(requestStructPointer->resourceTypeInt));
        QTableWidgetItem *requestUrlItemPointer = new QTableWidgetItem(requestStructPointer->urlString);

        // Create a request struct byte array.
        QByteArray *requestStructByteArrayPointer = new QByteArray();

        // Create a request struct data stream.
        QDataStream requestStructDataStream(requestStructByteArrayPointer, QIODevice::WriteOnly);

        // Populate the request struct data stream.
        requestStructDataStream << requestStructPointer->dispositionInt;
        requestStructDataStream << requestStructPointer->entryStruct.originalEntry;
        requestStructDataStream << requestStructPointer->entryStruct.originalFilterOptions;
        requestStructDataStream << requestStructPointer->entryStruct.appliedEntryList;
        requestStructDataStream << requestStructPointer->entryStruct.appliedFilterOptionsList;
        requestStructDataStream << requestStructPointer->entryStruct.domainList;
        requestStructDataStream << requestStructPointer->entryStruct.finalMatch;
        requestStructDataStream << requestStructPointer->entryStruct.hasRequestOptions;
        requestStructDataStream << requestStructPointer->entryStruct.initialMatch;
        requestStructDataStream << requestStructPointer->entryStruct.domain;
        requestStructDataStream << requestStructPointer->entryStruct.font;
        requestStructDataStream << requestStructPointer->entryStruct.image;
        requestStructDataStream << requestStructPointer->entryStruct.mainFrame;
        requestStructDataStream << requestStructPointer->entryStruct.media;
        requestStructDataStream << requestStructPointer->entryStruct.object;
        requestStructDataStream << requestStructPointer->entryStruct.other;
        requestStructDataStream << requestStructPointer->entryStruct.ping;
        requestStructDataStream << requestStructPointer->entryStruct.script;
        requestStructDataStream << requestStructPointer->entryStruct.styleSheet;
        requestStructDataStream << requestStructPointer->entryStruct.subFrame;
        requestStructDataStream << requestStructPointer->entryStruct.thirdParty;
        requestStructDataStream << requestStructPointer->entryStruct.xmlHttpRequest;
        requestStructDataStream << requestStructPointer->filterListTitle;
        requestStructDataStream << requestStructPointer->isThirdPartyRequest;
        requestStructDataStream << requestStructPointer->matchedUrlType;
        requestStructDataStream << requestStructPointer->navigationTypeInt;
        requestStructDataStream << requestStructPointer->requestMethodString;
        requestStructDataStream << requestStructPointer->resourceTypeInt;
        requestStructDataStream << requestStructPointer->sublistInt;
        requestStructDataStream << requestStructPointer->truncatedUrlString;
        requestStructDataStream << requestStructPointer->truncatedUrlStringWithSeparators;
        requestStructDataStream << requestStructPointer->urlString;
        requestStructDataStream << requestStructPointer->urlStringWithSeparators;
        requestStructDataStream << requestStructPointer->webPageUrlString;

        // Add the request struct to the disposition item.
        dispositionItemPointer->setData(Qt::UserRole, *requestStructByteArrayPointer);

        // Colorize the background according to the disposition.
        if (requestStructPointer->dispositionInt == FilterListHelper::BLOCKED)  // The request was blocked.
        {
            // Set the background to be red.
            dispositionItemPointer->setBackground(negativeBackgroundBrush);
            requestMethodItemPointer->setBackground(negativeBackgroundBrush);
            navigationTypeItemPointer->setBackground(negativeBackgroundBrush);
            resourceTypeItemPointer->setBackground(negativeBackgroundBrush);
            requestUrlItemPointer->setBackground(negativeBackgroundBrush);
        }
        else if (requestStructPointer->dispositionInt == FilterListHelper::ALLOWED)  // The request was allowed.
        {
            // Set the background to be green.
            dispositionItemPointer->setBackground(positiveBackgroundBrush);
            requestMethodItemPointer->setBackground(positiveBackgroundBrush);
            navigationTypeItemPointer->setBackground(positiveBackgroundBrush);
            resourceTypeItemPointer->setBackground(positiveBackgroundBrush);
            requestUrlItemPointer->setBackground(positiveBackgroundBrush);
        }

        // Add the entries to the table.
        tableWidgetPointer->setItem(rowCounter, 0, dispositionItemPointer);
        tableWidgetPointer->setItem(rowCounter, 1, requestMethodItemPointer);
        tableWidgetPointer->setItem(rowCounter, 2, navigationTypeItemPointer);
        tableWidgetPointer->setItem(rowCounter, 3, resourceTypeItemPointer);
        tableWidgetPointer->setItem(rowCounter, 4, requestUrlItemPointer);

        // Increment the row counter.
        ++rowCounter;
    }

    // Select an entire row at a time.
    tableWidgetPointer->setSelectionBehavior(QAbstractItemView::SelectRows);

    // Get the table header view.
    QHeaderView *headerViewPointer = tableWidgetPointer->horizontalHeader();

    // Resize the header to fit the contents.
    headerViewPointer->resizeSections(QHeaderView::ResizeToContents);

    // Connect changes in the sort order.
    connect(headerViewPointer, SIGNAL(sortIndicatorChanged(int, Qt::SortOrder)), this, SLOT(sortTable(int, Qt::SortOrder)));

    // Connect the buttons.
    connect(dialogButtonBoxPointer, SIGNAL(rejected()), this, SLOT(close()));

    // Open the request detail dialog when a cell is clicked.
    connect(tableWidgetPointer, SIGNAL(cellClicked(int, int)), this, SLOT(showRequestDetailDialog(int)));
}

void RequestsDialog::showRequestDetailDialog(int row)
{
    // Instantiate the request details dialog.
    RequestDetailDialog *requestDetailDialogPointer = new RequestDetailDialog(this, tableWidgetPointer, row);

    // Show the dialog.
    requestDetailDialogPointer->show();
}

void RequestsDialog::sortTable(int column, Qt::SortOrder sortOrder) const
{
    // Sort the table.
    tableWidgetPointer->sortItems(column, sortOrder);
}
