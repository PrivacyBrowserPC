/*
 * Copyright 2024 Soren Stoutner <soren@stoutner.com>.
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * Privacy Browser PC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Privacy Browser PC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Privacy Browser PC.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ENTRY_STRUCT_H
#define ENTRY_STRUCT_H

// Qt toolkit headers.
#include <QStringList>

enum FilterOptionDisposition
{
    Null = 0,
    Apply = 1,
    Override = 2,
};

struct EntryStruct
{
    // The strings.
    QString originalEntry;
    QString originalFilterOptions;

    // The string lists.
    QStringList appliedEntryList;
    QStringList appliedFilterOptionsList;
    QStringList domainList;

    // The booleans.
    bool finalMatch = false;
    bool hasRequestOptions = false;
    bool initialMatch = false;
    bool singleAppliedEntry = false;

    // The ints.
    int sizeOfAppliedEntryList;

    // The filter options.
    FilterOptionDisposition domain = FilterOptionDisposition::Null;
    FilterOptionDisposition font = FilterOptionDisposition::Null;
    FilterOptionDisposition image = FilterOptionDisposition::Null;
    FilterOptionDisposition mainFrame = FilterOptionDisposition::Null;
    FilterOptionDisposition media = FilterOptionDisposition::Null;
    FilterOptionDisposition object = FilterOptionDisposition::Null;
    FilterOptionDisposition other = FilterOptionDisposition::Null;
    FilterOptionDisposition ping = FilterOptionDisposition::Null;
    FilterOptionDisposition script = FilterOptionDisposition::Null;
    FilterOptionDisposition styleSheet = FilterOptionDisposition::Null;
    FilterOptionDisposition subFrame = FilterOptionDisposition::Null;
    FilterOptionDisposition thirdParty = FilterOptionDisposition::Null;
    FilterOptionDisposition xmlHttpRequest = FilterOptionDisposition::Null;
};
#endif
