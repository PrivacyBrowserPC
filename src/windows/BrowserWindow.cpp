/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022-2024 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Application headers.
#include "BrowserWindow.h"
#include "GlobalVariables.h"
#include "Settings.h"
#include "databases/BookmarksDatabase.h"
#include "databases/DomainsDatabase.h"
#include "dialogs/AddBookmarkDialog.h"
#include "dialogs/AddFolderDialog.h"
#include "dialogs/BookmarksDialog.h"
#include "dialogs/CookiesDialog.h"
#include "dialogs/DomainSettingsDialog.h"
#include "dialogs/EditBookmarkDialog.h"
#include "dialogs/EditFolderDialog.h"
#include "dialogs/FilterListsDialog.h"
#include "dialogs/RequestsDialog.h"
#include "dialogs/SettingsDialog.h"
#include "helpers/SearchEngineHelper.h"
#include "helpers/UserAgentHelper.h"
#include "structs/BookmarkStruct.h"

// Qt toolkit headers.
#include <QActionGroup>
#include <QClipboard>
#include <QContextMenuEvent>
#include <QDBusConnection>
#include <QDBusConnectionInterface>
#include <QDBusMessage>
#include <QGuiApplication>
#include <QInputDialog>
#include <QLayout>
#include <QNetworkCookie>
#include <QMenuBar>
#include <QMessageBox>
#include <QShortcut>
#include <QStatusBar>
#include <QWebEngineFindTextResult>

// KDE Frameworks headers.
#include <KActionCollection>
#include <KColorScheme>
#include <KLocalizedString>
#include <KXMLGUIFactory>

// Construct the class.
BrowserWindow::BrowserWindow(bool firstWindow, QString *initialUrlStringPointer) : KXmlGuiWindow()
{
    // Initialize the variables.
    javaScriptEnabled = false;
    localStorageEnabled = false;

    // Instantiate the privacy tab widget pointer.
    tabWidgetPointer = new TabWidget(this);

    // Set the privacy tab widget as the central widget.
    setCentralWidget(tabWidgetPointer);

    // Get a handle for the action collection.
    actionCollectionPointer = this->actionCollection();

    // Add the standard actions.
    KStandardAction::print(tabWidgetPointer, SLOT(print()), actionCollectionPointer);
    QAction *printPreviewActionPointer = KStandardAction::printPreview(tabWidgetPointer, SLOT(printPreview()), actionCollectionPointer);
    KStandardAction::quit(qApp, SLOT(closeAllWindows()), actionCollectionPointer);
    zoomInActionPointer = KStandardAction::zoomIn(this, SLOT(incrementZoom()), actionCollectionPointer);
    zoomOutActionPointer = KStandardAction::zoomOut(this, SLOT(decrementZoom()), actionCollectionPointer);
    refreshActionPointer = KStandardAction::redisplay(this, SLOT(refresh()), actionCollectionPointer);
    fullScreenActionPointer = KStandardAction::fullScreen(this, SLOT(toggleFullScreen()), this, actionCollectionPointer);
    QAction *backActionPointer = KStandardAction::back(this, SLOT(back()), actionCollectionPointer);
    QAction *forwardActionPointer = KStandardAction::forward(this, SLOT(forward()), actionCollectionPointer);
    KStandardAction::home(this, SLOT(home()), actionCollectionPointer);
    QAction *editBookmarksActionPointer = KStandardAction::editBookmarks(this, SLOT(editBookmarks()), actionCollectionPointer);
    KStandardAction::preferences(this, SLOT(showSettingsDialog()), actionCollectionPointer);
    KStandardAction::find(this, SLOT(showFindTextActions()), actionCollectionPointer);
    findNextActionPointer = KStandardAction::findNext(this, SLOT(findNext()), actionCollectionPointer);
    findPreviousActionPointer = KStandardAction::findPrev(this, SLOT(findPrevious()), actionCollectionPointer);

    // Add the custom actions.
    QAction *newTabActionPointer = actionCollectionPointer->addAction(QLatin1String("new_tab"));
    QAction *newWindowActionPointer = actionCollectionPointer->addAction(QLatin1String("new_window"));
    QAction *saveArchiveActionPointer = actionCollectionPointer->addAction(QLatin1String("save_archive"));
    QAction *openWithFirefoxActionPointer = actionCollectionPointer->addAction(QLatin1String("open_with_firefox"));
    QAction *openWithChromiumActionPointer = actionCollectionPointer->addAction(QLatin1String("open_with_chromium"));
    zoomDefaultActionPointer = actionCollectionPointer->addAction(QLatin1String("zoom_default"));
    QAction *reloadAndBypassCacheActionPointer = actionCollectionPointer->addAction(QLatin1String("reload_and_bypass_cache"));
    stopActionPointer = actionCollectionPointer->addAction(QLatin1String("stop"));
    viewSourceActionPointer = actionCollectionPointer->addAction(QLatin1String("view_source"));
    viewSourceInNewTabActionPointer = actionCollectionPointer->addAction(QLatin1String("view_source_in_new_tab"));
    developerToolsActionPointer = actionCollectionPointer->addAction(QLatin1String("developer_tools"));
    javaScriptActionPointer = actionCollectionPointer->addAction(QLatin1String("javascript"));
    localStorageActionPointer = actionCollectionPointer->addAction(QLatin1String("local_storage"));
    domStorageActionPointer = actionCollectionPointer->addAction(QLatin1String("dom_storage"));
    userAgentPrivacyBrowserActionPointer = actionCollectionPointer->addAction(QLatin1String("user_agent_privacy_browser"));
    userAgentWebEngineDefaultActionPointer = actionCollectionPointer->addAction(QLatin1String("user_agent_webengine_default"));
    userAgentFirefoxLinuxActionPointer = actionCollectionPointer->addAction(QLatin1String("user_agent_firefox_linux"));
    userAgentChromiumLinuxActionPointer = actionCollectionPointer->addAction(QLatin1String("user_agent_chromium_linux"));
    userAgentFirefoxWindowsActionPointer = actionCollectionPointer->addAction(QLatin1String("user_agent_firefox_windows"));
    userAgentChromeWindowsActionPointer = actionCollectionPointer->addAction(QLatin1String("user_agent_chrome_windows"));
    userAgentEdgeWindowsActionPointer = actionCollectionPointer->addAction(QLatin1String("user_agent_edge_windows"));
    userAgentSafariMacosActionPointer = actionCollectionPointer->addAction(QLatin1String("user_agent_safari_macos"));
    userAgentCustomActionPointer = actionCollectionPointer->addAction(QLatin1String("user_agent_custom"));
    zoomFactorActionPointer = actionCollectionPointer->addAction(QLatin1String("zoom_factor"));
    searchEngineMojeekActionPointer = actionCollectionPointer->addAction(QLatin1String("search_engine_mojeek"));
    searchEngineMonoclesActionPointer = actionCollectionPointer->addAction(QLatin1String("search_engine_monocles"));
    searchEngineMetagerActionPointer = actionCollectionPointer->addAction(QLatin1String("search_engine_metager"));
    searchEngineGoogleActionPointer = actionCollectionPointer->addAction(QLatin1String("search_engine_google"));
    searchEngineBingActionPointer = actionCollectionPointer->addAction(QLatin1String("search_engine_bing"));
    searchEngineYahooActionPointer = actionCollectionPointer->addAction(QLatin1String("search_engine_yahoo"));
    searchEngineCustomActionPointer = actionCollectionPointer->addAction(QLatin1String("search_engine_custom"));
    ultraPrivacyActionPointer = actionCollectionPointer->addAction(QLatin1String("ultraprivacy"));
    ultraListActionPointer = actionCollectionPointer->addAction(QLatin1String("ultralist"));
    easyPrivacyActionPointer = actionCollectionPointer->addAction(QLatin1String("easyprivacy"));
    easyListActionPointer = actionCollectionPointer->addAction(QLatin1String("easylist"));
    fanboysAnnoyanceListPointer = actionCollectionPointer->addAction(QLatin1String("fanboys_annoyance_list"));
    requestsActionPointer = actionCollectionPointer->addAction(QLatin1String("requests"));
    QAction *viewFilterListsActionPointer = actionCollectionPointer->addAction(QLatin1String("view_filter_lists"));
    viewBookmarksToolBarActionPointer = actionCollectionPointer->addAction(QLatin1String("view_bookmarks_toolbar"));
    QAction *domainSettingsActionPointer = actionCollectionPointer->addAction(QLatin1String("domain_settings"));
    cookiesActionPointer = actionCollectionPointer->addAction(QLatin1String("cookies"));
    blockedRequestsActionPointer = actionCollectionPointer->addAction(QLatin1String("blocked_requests"));
    findCaseSensitiveActionPointer = actionCollectionPointer->addAction(QLatin1String("find_case_sensitive"));
    hideFindTextActionPointer = actionCollectionPointer->addAction(QLatin1String("hide_find_actions"));

    // Create the action groups
    QActionGroup *userAgentActionGroupPointer = new QActionGroup(this);
    QActionGroup *searchEngineActionGroupPointer = new QActionGroup(this);

    // Add the actions to the groups.
    userAgentActionGroupPointer->addAction(userAgentPrivacyBrowserActionPointer);
    userAgentActionGroupPointer->addAction(userAgentWebEngineDefaultActionPointer);
    userAgentActionGroupPointer->addAction(userAgentFirefoxLinuxActionPointer);
    userAgentActionGroupPointer->addAction(userAgentChromiumLinuxActionPointer);
    userAgentActionGroupPointer->addAction(userAgentFirefoxWindowsActionPointer);
    userAgentActionGroupPointer->addAction(userAgentChromeWindowsActionPointer);
    userAgentActionGroupPointer->addAction(userAgentEdgeWindowsActionPointer);
    userAgentActionGroupPointer->addAction(userAgentSafariMacosActionPointer);
    userAgentActionGroupPointer->addAction(userAgentCustomActionPointer);
    searchEngineActionGroupPointer->addAction(searchEngineMojeekActionPointer);
    searchEngineActionGroupPointer->addAction(searchEngineMonoclesActionPointer);
    searchEngineActionGroupPointer->addAction(searchEngineMetagerActionPointer);
    searchEngineActionGroupPointer->addAction(searchEngineGoogleActionPointer);
    searchEngineActionGroupPointer->addAction(searchEngineBingActionPointer);
    searchEngineActionGroupPointer->addAction(searchEngineYahooActionPointer);
    searchEngineActionGroupPointer->addAction(searchEngineCustomActionPointer);

    // Set some actions to be checkable.
    javaScriptActionPointer->setCheckable(true);
    localStorageActionPointer->setCheckable(true);
    domStorageActionPointer->setCheckable(true);
    findCaseSensitiveActionPointer->setCheckable(true);
    viewSourceActionPointer->setCheckable(true);
    developerToolsActionPointer->setCheckable(true);
    userAgentPrivacyBrowserActionPointer->setCheckable(true);
    userAgentWebEngineDefaultActionPointer->setCheckable(true);
    userAgentFirefoxLinuxActionPointer->setCheckable(true);
    userAgentChromiumLinuxActionPointer->setCheckable(true);
    userAgentFirefoxWindowsActionPointer->setCheckable(true);
    userAgentChromeWindowsActionPointer->setCheckable(true);
    userAgentEdgeWindowsActionPointer->setCheckable(true);
    userAgentSafariMacosActionPointer->setCheckable(true);
    userAgentCustomActionPointer->setCheckable(true);
    searchEngineMojeekActionPointer->setCheckable(true);
    searchEngineMonoclesActionPointer->setCheckable(true);
    searchEngineMetagerActionPointer->setCheckable(true);
    searchEngineGoogleActionPointer->setCheckable(true);
    searchEngineBingActionPointer->setCheckable(true);
    searchEngineYahooActionPointer->setCheckable(true);
    searchEngineCustomActionPointer->setCheckable(true);
    ultraPrivacyActionPointer->setCheckable(true);
    ultraListActionPointer->setCheckable(true);
    easyPrivacyActionPointer->setCheckable(true);
    easyListActionPointer->setCheckable(true);
    fanboysAnnoyanceListPointer->setCheckable(true);
    viewBookmarksToolBarActionPointer->setCheckable(true);

    // Instantiate the user agent helper.
    UserAgentHelper *userAgentHelperPointer = new UserAgentHelper();

    // Set the action text.
    newTabActionPointer->setText(i18nc("New tab action", "New Tab"));
    newWindowActionPointer->setText(i18nc("New window action", "New Window"));
    saveArchiveActionPointer->setText(i18nc("Save archive action", "Save Archive"));
    openWithFirefoxActionPointer->setText(i18nc("Open with Firefox action", "Open With Firefox"));
    openWithChromiumActionPointer->setText(i18nc("Open with Chromium action", "Open With Chromium"));
    zoomDefaultActionPointer->setText(i18nc("Zoom default action", "Zoom Default"));
    reloadAndBypassCacheActionPointer->setText(i18nc("Reload and bypass cache action", "Reload and Bypass Cache"));
    stopActionPointer->setText(i18nc("Stop action", "Stop"));
    viewSourceActionPointer->setText(i18nc("View source action", "View Source"));
    viewSourceInNewTabActionPointer->setText(i18nc("View source in new tab action", "View Source in New Tab"));
    developerToolsActionPointer->setText(i18nc("Developer tools action", "Developer Tools"));
    javaScriptActionPointer->setText(i18nc("JavaScript action", "JavaScript"));
    localStorageActionPointer->setText(i18nc("Local Storage action", "Local Storage"));
    domStorageActionPointer->setText(i18nc("DOM Storage action", "DOM Storage"));
    userAgentPrivacyBrowserActionPointer->setText(userAgentHelperPointer->PRIVACY_BROWSER_TRANSLATED);
    userAgentWebEngineDefaultActionPointer->setText(userAgentHelperPointer->WEB_ENGINE_DEFAULT_TRANSLATED);
    userAgentFirefoxLinuxActionPointer->setText(userAgentHelperPointer->FIREFOX_LINUX_TRANSLATED);
    userAgentChromiumLinuxActionPointer->setText(userAgentHelperPointer->CHROMIUM_LINUX_TRANSLATED);
    userAgentFirefoxWindowsActionPointer->setText(userAgentHelperPointer->FIREFOX_WINDOWS_TRANSLATED);
    userAgentChromeWindowsActionPointer->setText(userAgentHelperPointer->CHROME_WINDOWS_TRANSLATED);
    userAgentEdgeWindowsActionPointer->setText(userAgentHelperPointer->EDGE_WINDOWS_TRANSLATED);
    userAgentSafariMacosActionPointer->setText(userAgentHelperPointer->SAFARI_MACOS_TRANSLATED);
    searchEngineMojeekActionPointer->setText(i18nc("Mojeek search engine action", "Mojeek"));
    searchEngineMonoclesActionPointer->setText(i18nc("Monocles search engine action", "Monocles"));
    searchEngineMetagerActionPointer->setText(i18nc("MetaGer search engine action", "MetaGer"));
    searchEngineGoogleActionPointer->setText(i18nc("Google search engine action", "Google"));
    searchEngineBingActionPointer->setText(i18nc("Bing search engine action", "Bing"));
    searchEngineYahooActionPointer->setText(i18nc("Yahoo search engine action", "Yahoo"));
    ultraPrivacyActionPointer->setText(i18nc("UltraPrivacy action", "UltraPrivacy - %1 Blocked", 0));
    ultraListActionPointer->setText(i18nc("UltraList action", "UltraList - %1 Blocked", 0));
    easyPrivacyActionPointer->setText(i18nc("EasyPrivacy action", "EasyPrivacy - %1 Blocked", 0));
    easyListActionPointer->setText(i18nc("EasyList action", "EasyList - %1 Blocked", 0));
    fanboysAnnoyanceListPointer->setText(i18nc("Fanboy's Annoyance List action", "Fanboy’s Annoyance List - %1 Blocked", 0));
    requestsActionPointer->setText(i18nc("Requests action", "Requests - %1 Blocked", 0));
    viewFilterListsActionPointer->setText(i18nc("View filter lists action", "View Filter Lists"));
    viewBookmarksToolBarActionPointer->setText(i18nc("View bookmarks toolbar action", "View Bookmarks Toolbar"));
    domainSettingsActionPointer->setText(i18nc("Domain Settings action", "Domain Settings"));
    cookiesActionPointer->setText(i18nc("Cookies action, which also displays the number of cookies", "Cookies - %1", 0));
    blockedRequestsActionPointer->setText(QLatin1String("0"));
    findCaseSensitiveActionPointer->setText(i18nc("Find Case Sensitive action", "Find Case Sensitive"));
    hideFindTextActionPointer->setText(i18nc("Hide Find Text action (the text should include the language-specific escape keyboard shortcut).", "Hide Find Text (Esc)"));

    // Set the tool tips.
    blockedRequestsActionPointer->setToolTip(i18nc("Blocked requests action tool tip", "%1 Blocked Requests (Ctrl+R)", 0));

    // Set the action icons.  Gnome doesn't contain some of the icons that KDE has.
    // The toolbar icons don't pick up unless the size is explicit, probably because the toolbar ends up being an intermediate size.
    newTabActionPointer->setIcon(QIcon::fromTheme(QLatin1String("tab-new")));
    newWindowActionPointer->setIcon(QIcon::fromTheme(QLatin1String("window-new")));
    saveArchiveActionPointer->setIcon(QIcon::fromTheme(QLatin1String("document-save")));
    openWithFirefoxActionPointer->setIcon(QIcon::fromTheme(QLatin1String("firefox-esr")));
    openWithChromiumActionPointer->setIcon(QIcon::fromTheme(QLatin1String("chromium")));
    zoomDefaultActionPointer->setIcon(QIcon::fromTheme(QLatin1String("zoom-fit-best")));
    reloadAndBypassCacheActionPointer->setIcon(QIcon::fromTheme(QLatin1String("view-refresh")));
    stopActionPointer->setIcon(QIcon::fromTheme(QLatin1String("process-stop")));
    viewSourceActionPointer->setIcon(QIcon::fromTheme(QLatin1String("view-choose"), QIcon::fromTheme(QLatin1String("accessories-text-editor"))));
    viewSourceInNewTabActionPointer->setIcon(QIcon::fromTheme(QLatin1String("view-choose"), QIcon::fromTheme(QLatin1String("accessories-text-editor"))));
    developerToolsActionPointer->setIcon(QIcon::fromTheme(QLatin1String("add-subtitle"), QIcon::fromTheme(QLatin1String("system-run"))));
    domStorageActionPointer->setIcon(QIcon::fromTheme(QLatin1String("code-class"), QIcon(QLatin1String("/usr/share/icons/gnome/32x32/actions/gtk-unindent-ltr.png"))));
    userAgentPrivacyBrowserActionPointer->setIcon(QIcon(QLatin1String(":/icons/privacy-mode.svg")));
    userAgentWebEngineDefaultActionPointer->setIcon(QIcon::fromTheme(QLatin1String("qtlogo"), QIcon::fromTheme(QLatin1String("user-group-properties"),
                                                                                                               QIcon::fromTheme(QLatin1String("contact-new")))));
    userAgentFirefoxLinuxActionPointer->setIcon(QIcon::fromTheme(QLatin1String("firefox-esr"), QIcon::fromTheme(QLatin1String("user-group-properties"),
                                                                                                                QIcon::fromTheme(QLatin1String("contact-new")))));
    userAgentChromiumLinuxActionPointer->setIcon(QIcon::fromTheme(QLatin1String("chromium"), QIcon::fromTheme(QLatin1String("user-group-properties"), QIcon::fromTheme(QLatin1String("contact-new")))));
    userAgentFirefoxWindowsActionPointer->setIcon(QIcon::fromTheme(QLatin1String("firefox-esr"), QIcon::fromTheme(QLatin1String("user-group-properties"),
                                                                                                                  QIcon::fromTheme(QLatin1String("contact-new")))));
    userAgentChromeWindowsActionPointer->setIcon(QIcon::fromTheme(QLatin1String("chromium"), QIcon::fromTheme(QLatin1String("user-group-properties"), QIcon::fromTheme(QLatin1String("contact-new")))));
    userAgentEdgeWindowsActionPointer->setIcon(QIcon::fromTheme(QLatin1String("user-group-properties"), QIcon::fromTheme(QLatin1String("contact-new"))));
    userAgentSafariMacosActionPointer->setIcon(QIcon::fromTheme(QLatin1String("user-group-properties"), QIcon::fromTheme(QLatin1String("contact-new"))));
    userAgentCustomActionPointer->setIcon(QIcon::fromTheme(QLatin1String("user-group-properties"), QIcon::fromTheme(QLatin1String("contact-new"))));
    zoomFactorActionPointer->setIcon(QIcon::fromTheme(QLatin1String("zoom-fit-best")));
    searchEngineMojeekActionPointer->setIcon(QIcon::fromTheme(QLatin1String("edit-find")));
    searchEngineMonoclesActionPointer->setIcon(QIcon::fromTheme(QLatin1String("edit-find")));
    searchEngineMetagerActionPointer->setIcon(QIcon::fromTheme(QLatin1String("edit-find")));
    searchEngineGoogleActionPointer->setIcon(QIcon::fromTheme(QLatin1String("im-google"), QIcon::fromTheme(QLatin1String("edit-find"))));
    searchEngineBingActionPointer->setIcon(QIcon::fromTheme(QLatin1String("edit-find")));
    searchEngineYahooActionPointer->setIcon(QIcon::fromTheme(QLatin1String("im-yahoo"), QIcon::fromTheme(QLatin1String("edit-find"))));
    searchEngineCustomActionPointer->setIcon(QIcon::fromTheme(QLatin1String("edit-find")));
    requestsActionPointer->setIcon(QIcon::fromTheme(QLatin1String("document-equal"), QIcon::fromTheme(QLatin1String("system-lock-screen"))));
    ultraPrivacyActionPointer->setIcon(QIcon::fromTheme(QLatin1String("view-list-text"), QIcon::fromTheme(QLatin1String("format-justify-left"))));
    ultraListActionPointer->setIcon(QIcon::fromTheme(QLatin1String("view-list-text"), QIcon::fromTheme(QLatin1String("format-justify-left"))));
    easyPrivacyActionPointer->setIcon(QIcon::fromTheme(QLatin1String("view-list-text"), QIcon::fromTheme(QLatin1String("format-justify-left"))));
    easyListActionPointer->setIcon(QIcon::fromTheme(QLatin1String("view-list-text"), QIcon::fromTheme(QLatin1String("format-justify-left"))));
    fanboysAnnoyanceListPointer->setIcon(QIcon::fromTheme(QLatin1String("view-list-text"), QIcon::fromTheme(QLatin1String("format-justify-left"))));
    viewFilterListsActionPointer->setIcon(QIcon::fromTheme(QLatin1String("view-list-text"), QIcon::fromTheme(QLatin1String("format-justify-left"))));
    editBookmarksActionPointer->setIcon(QIcon::fromTheme(QLatin1String("bookmark-edit"), QIcon::fromTheme(QLatin1String("bookmark-new"))));
    viewBookmarksToolBarActionPointer->setIcon(QIcon::fromTheme(QLatin1String("bookmarks"), QIcon::fromTheme(QLatin1String("bookmark-new"))));
    domainSettingsActionPointer->setIcon(QIcon::fromTheme(QLatin1String("settings-configure"), QIcon::fromTheme(QLatin1String("preferences-desktop"))));
    cookiesActionPointer->setIcon(QIcon::fromTheme(QLatin1String("preferences-web-browser-cookies"), QIcon::fromTheme(QLatin1String("appointment-new"))));
    findCaseSensitiveActionPointer->setIcon(QIcon::fromTheme(QLatin1String("format-text-lowercase"), QIcon::fromTheme(QLatin1String("/usr/share/icons/gnome/32x32/apps/fonts.png"))));
    hideFindTextActionPointer->setIcon(QIcon::fromTheme(QLatin1String("window-close-symbolic")));

    // Create the key sequences.
    QKeySequence ctrlTKeySequence = QKeySequence(i18nc("The open new tab key sequence.", "Ctrl+T"));
    QKeySequence ctrlNKeySequence = QKeySequence(i18nc("The open new window key sequence.", "Ctrl+N"));
    QKeySequence ctrlAKeySequence = QKeySequence(i18nc("The save archive key sequence.", "Ctrl+A"));
    QKeySequence altFKeySequence = QKeySequence(i18nc("The open with Firefox key sequence.", "Alt+F"));
    QKeySequence altCKeySequence = QKeySequence(i18nc("The open with Chromium key sequence.", "Alt+C"));
    QKeySequence ctrl0KeySequence = QKeySequence(i18nc("The zoom default key sequence.", "Ctrl+0"));
    QKeySequence ctrlF5KeySequence = QKeySequence(i18nc("The reload and bypass cache key sequence.", "Ctrl+F5"));
    QKeySequence ctrlShiftXKeySequence = QKeySequence(i18nc("The stop key sequence.", "Ctrl+Shift+X"));
    QKeySequence ctrlUKeySequence = QKeySequence(i18nc("The view source key sequence.", "Ctrl+U"));
    QKeySequence ctrlShiftUKeySequence = QKeySequence(i18nc("The view source in new tab key sequence.", "Ctrl+Shift+U"));
    QKeySequence f12KeySequence = QKeySequence(i18nc("The developer tools key sequence.", "F12"));
    QKeySequence ctrlShiftPKeySequence = QKeySequence(i18nc("The print preview key sequence.", "Ctrl+Shift+P"));
    QKeySequence ctrlJKeySequence = QKeySequence(i18nc("The JavaScript key sequence.", "Ctrl+J"));
    QKeySequence ctrlLKeySequence = QKeySequence(i18nc("The local storage key sequence.", "Ctrl+L"));
    QKeySequence ctrlDKeySequence = QKeySequence(i18nc("The DOM storage key sequence.", "Ctrl+D"));
    QKeySequence ctrlSKeySequence = QKeySequence(i18nc("The find case sensitive key sequence.", "Ctrl+S"));
    QKeySequence ctrlAltPKeySequence = QKeySequence(i18nc("The Privacy Browser user agent key sequence.", "Ctrl+Alt+P"));
    QKeySequence ctrlAltWKeySequence = QKeySequence(i18nc("The WebEngine Default user agent key sequence.", "Ctrl+Alt+W"));
    QKeySequence ctrlAltFKeySequence = QKeySequence(i18nc("The Firefox on Linux user agent key sequence.", "Ctrl+Alt+F"));
    QKeySequence ctrlAltCKeySequence = QKeySequence(i18nc("The Chromium on Linux user agent key sequence.", "Ctrl+Alt+C"));
    QKeySequence ctrlAltShiftFKeySequence = QKeySequence(i18nc("The Firefox on Windows user agent key sequence.", "Ctrl+Alt+Shift+F"));
    QKeySequence ctrlAltShiftCKeySequence = QKeySequence(i18nc("The Chrome on Windows user agent key sequence.", "Ctrl+Alt+Shift+C"));
    QKeySequence ctrlAltEKeySequence = QKeySequence(i18nc("The Edge on Windows user agent key sequence.", "Ctrl+Alt+E"));
    QKeySequence ctrlAltSKeySequence = QKeySequence(i18nc("The Safari on macOS user agent key sequence.", "Ctrl+Alt+S"));
    QKeySequence altShiftCKeySequence = QKeySequence(i18nc("The custom user agent key sequence.", "Alt+Shift+C"));
    QKeySequence ctrlAltZKeySequence = QKeySequence(i18nc("The zoom factor key sequence.", "Ctrl+Alt+Z"));
    QKeySequence ctrlShiftMKeySequence = QKeySequence(i18nc("The Mojeek search engine key sequence.", "Ctrl+Shift+M"));
    QKeySequence ctrlShiftOKeySequence = QKeySequence(i18nc("The Monocles search engine key sequence.", "Ctrl+Shift+O"));
    QKeySequence ctrlShiftEKeySequence = QKeySequence(i18nc("The MetaGer search engine key sequence.", "Ctrl+Shift+E"));
    QKeySequence ctrlShiftGKeySequence = QKeySequence(i18nc("The Google search engine key sequence.", "Ctrl+Shift+G"));
    QKeySequence ctrlShiftBKeySequence = QKeySequence(i18nc("The Bing search engine key sequence.", "Ctrl+Shift+B"));
    QKeySequence ctrlShiftYKeySequence = QKeySequence(i18nc("The Yahoo search engine key sequence.", "Ctrl+Shift+Y"));
    QKeySequence ctrlShiftCKeySequence = QKeySequence(i18nc("The custom search engine key sequence.", "Ctrl+Shift+C"));
    QKeySequence ctrlRKeySequence = QKeySequence(i18nc("The requests key sequence.", "Ctrl+R"));
    QKeySequence ctrlShiftLKeySequence = QKeySequence(i18nc("The view filter lists key sequence.", "Ctrl+Shift+L"));
    QKeySequence ctrlAltShiftBKeySequence = QKeySequence(i18nc("The edit bookmarks key sequence.", "Ctrl+Alt+Shift+B"));
    QKeySequence ctrlAltBKeySequence = QKeySequence(i18nc("The view bookmarks toolbar key sequence.", "Ctrl+Alt+B"));
    QKeySequence ctrlShiftDKeySequence = QKeySequence(i18nc("The domain settings key sequence.", "Ctrl+Shift+D"));
    QKeySequence ctrlSemicolonKeySequence = QKeySequence(i18nc("The cookies dialog key sequence.", "Ctrl+;"));

    // Set the action key sequences.
    actionCollectionPointer->setDefaultShortcut(newTabActionPointer, ctrlTKeySequence);
    actionCollectionPointer->setDefaultShortcut(newWindowActionPointer, ctrlNKeySequence);
    actionCollectionPointer->setDefaultShortcut(saveArchiveActionPointer, ctrlAKeySequence);
    actionCollectionPointer->setDefaultShortcut(openWithFirefoxActionPointer, altFKeySequence);
    actionCollectionPointer->setDefaultShortcut(openWithChromiumActionPointer, altCKeySequence);
    actionCollectionPointer->setDefaultShortcut(zoomDefaultActionPointer, ctrl0KeySequence);
    actionCollectionPointer->setDefaultShortcut(reloadAndBypassCacheActionPointer, ctrlF5KeySequence);
    actionCollectionPointer->setDefaultShortcut(stopActionPointer, ctrlShiftXKeySequence);
    actionCollectionPointer->setDefaultShortcut(viewSourceActionPointer, ctrlUKeySequence);
    actionCollectionPointer->setDefaultShortcut(viewSourceInNewTabActionPointer, ctrlShiftUKeySequence);
    actionCollectionPointer->setDefaultShortcut(developerToolsActionPointer, f12KeySequence);
    actionCollectionPointer->setDefaultShortcut(printPreviewActionPointer, ctrlShiftPKeySequence);
    actionCollectionPointer->setDefaultShortcut(javaScriptActionPointer, ctrlJKeySequence);
    actionCollectionPointer->setDefaultShortcut(localStorageActionPointer, ctrlLKeySequence);
    actionCollectionPointer->setDefaultShortcut(domStorageActionPointer, ctrlDKeySequence);
    actionCollectionPointer->setDefaultShortcut(findCaseSensitiveActionPointer, ctrlSKeySequence);
    actionCollectionPointer->setDefaultShortcut(userAgentPrivacyBrowserActionPointer, ctrlAltPKeySequence);
    actionCollectionPointer->setDefaultShortcut(userAgentWebEngineDefaultActionPointer, ctrlAltWKeySequence);
    actionCollectionPointer->setDefaultShortcut(userAgentFirefoxLinuxActionPointer, ctrlAltFKeySequence);
    actionCollectionPointer->setDefaultShortcut(userAgentChromiumLinuxActionPointer, ctrlAltCKeySequence);
    actionCollectionPointer->setDefaultShortcut(userAgentFirefoxWindowsActionPointer, ctrlAltShiftFKeySequence);
    actionCollectionPointer->setDefaultShortcut(userAgentChromeWindowsActionPointer, ctrlAltShiftCKeySequence);
    actionCollectionPointer->setDefaultShortcut(userAgentEdgeWindowsActionPointer, ctrlAltEKeySequence);
    actionCollectionPointer->setDefaultShortcut(userAgentSafariMacosActionPointer, ctrlAltSKeySequence);
    actionCollectionPointer->setDefaultShortcut(userAgentCustomActionPointer, altShiftCKeySequence);
    actionCollectionPointer->setDefaultShortcut(zoomFactorActionPointer, ctrlAltZKeySequence);
    actionCollectionPointer->setDefaultShortcut(searchEngineMojeekActionPointer, ctrlShiftMKeySequence);
    actionCollectionPointer->setDefaultShortcut(searchEngineMonoclesActionPointer, ctrlShiftOKeySequence);
    actionCollectionPointer->setDefaultShortcut(searchEngineMetagerActionPointer, ctrlShiftEKeySequence);
    actionCollectionPointer->setDefaultShortcut(searchEngineGoogleActionPointer, ctrlShiftGKeySequence);
    actionCollectionPointer->setDefaultShortcut(searchEngineBingActionPointer, ctrlShiftBKeySequence);
    actionCollectionPointer->setDefaultShortcut(searchEngineYahooActionPointer, ctrlShiftYKeySequence);
    actionCollectionPointer->setDefaultShortcut(searchEngineCustomActionPointer, ctrlShiftCKeySequence);
    actionCollectionPointer->setDefaultShortcut(requestsActionPointer, ctrlRKeySequence);
    actionCollectionPointer->setDefaultShortcut(viewFilterListsActionPointer, ctrlShiftLKeySequence);
    actionCollectionPointer->setDefaultShortcut(editBookmarksActionPointer, ctrlAltShiftBKeySequence);
    actionCollectionPointer->setDefaultShortcut(viewBookmarksToolBarActionPointer, ctrlAltBKeySequence);
    actionCollectionPointer->setDefaultShortcut(domainSettingsActionPointer, ctrlShiftDKeySequence);
    actionCollectionPointer->setDefaultShortcut(cookiesActionPointer, ctrlSemicolonKeySequence);

    // Connect the actions.
    connect(newTabActionPointer, SIGNAL(triggered()), tabWidgetPointer, SLOT(addTab()));
    connect(newWindowActionPointer, SIGNAL(triggered()), this, SLOT(newWindow()));
    connect(saveArchiveActionPointer, SIGNAL(triggered()), tabWidgetPointer, SLOT(saveArchive()));
    connect(openWithFirefoxActionPointer, SIGNAL(triggered()), this, SLOT(openWithFirefox()));
    connect(openWithChromiumActionPointer, SIGNAL(triggered()), this, SLOT(openWithChromium()));
    connect(zoomDefaultActionPointer, SIGNAL(triggered()), this, SLOT(zoomDefault()));
    connect(reloadAndBypassCacheActionPointer, SIGNAL(triggered()), this, SLOT(reloadAndBypassCache()));
    connect(stopActionPointer, SIGNAL(triggered()), tabWidgetPointer, SLOT(stop()));
    connect(viewSourceActionPointer, SIGNAL(triggered()), this, SLOT(toggleViewSource()));
    connect(viewSourceInNewTabActionPointer, SIGNAL(triggered()), this, SLOT(toggleViewSourceInNewTab()));
    connect(developerToolsActionPointer, SIGNAL(triggered()), this, SLOT(toggleDeveloperTools()));
    connect(zoomFactorActionPointer, SIGNAL(triggered()), this, SLOT(getZoomFactorFromUser()));
    connect(requestsActionPointer, SIGNAL(triggered()), this, SLOT(showRequestsDialog()));
    connect(viewFilterListsActionPointer, SIGNAL(triggered()), this, SLOT(showFilterListsDialog()));
    connect(viewBookmarksToolBarActionPointer, SIGNAL(triggered()), this, SLOT(toggleViewBookmarksToolBar()));
    connect(cookiesActionPointer, SIGNAL(triggered()), this, SLOT(showCookiesDialog()));
    connect(domainSettingsActionPointer, SIGNAL(triggered()), this, SLOT(showDomainSettingsDialog()));
    connect(blockedRequestsActionPointer, SIGNAL(triggered()), this, SLOT(showRequestsDialog()));

    // Update the on-the-fly menus.
    connect(tabWidgetPointer, SIGNAL(updateUserAgentActions(QString, bool)), this, SLOT(updateUserAgentActions(QString, bool)));
    connect(tabWidgetPointer, SIGNAL(updateZoomActions(double)), this, SLOT(updateZoomActions(double)));
    connect(tabWidgetPointer, SIGNAL(updateSearchEngineActions(QString, bool)), this, SLOT(updateSearchEngineActions(QString, bool)));

    // Apply the on-the-fly settings when selected.
    connect(userAgentActionGroupPointer, SIGNAL(triggered(QAction*)), tabWidgetPointer, SLOT(applyOnTheFlyUserAgent(QAction*)));
    connect(searchEngineActionGroupPointer, SIGNAL(triggered(QAction*)), tabWidgetPointer, SLOT(applyOnTheFlySearchEngine(QAction*)));

    // Process blocked requests.
    connect(tabWidgetPointer, SIGNAL(blockedRequestsUpdated(const QVector<int>)), this, SLOT(updateRequestsAction(const QVector<int>)));

    // Process cookie changes.
    connect(tabWidgetPointer, SIGNAL(cookiesChanged(const int)), this, SLOT(updateCookiesAction(const int)));

    // Store the default zoom factor.
    connect(tabWidgetPointer, SIGNAL(updateDefaultZoomFactor(double)), this, SLOT(updateDefaultZoomFactor(double)));

    // Connect the filter list actions.
    connect(ultraPrivacyActionPointer, SIGNAL(triggered()), tabWidgetPointer, SLOT(toggleUltraPrivacy()));
    connect(ultraListActionPointer, SIGNAL(triggered()), tabWidgetPointer, SLOT(toggleUltraList()));
    connect(easyPrivacyActionPointer, SIGNAL(triggered()), tabWidgetPointer, SLOT(toggleEasyPrivacy()));
    connect(easyListActionPointer, SIGNAL(triggered()), tabWidgetPointer, SLOT(toggleEasyList()));
    connect(fanboysAnnoyanceListPointer, SIGNAL(triggered()), tabWidgetPointer, SLOT(toggleFanboysAnnoyanceList()));

    // Connect the URL toolbar actions.
    connect(javaScriptActionPointer, SIGNAL(triggered()), this, SLOT(toggleJavaScript()));
    connect(localStorageActionPointer, SIGNAL(triggered()), this, SLOT(toggleLocalStorage()));
    connect(domStorageActionPointer, SIGNAL(triggered()), this, SLOT(toggleDomStorage()));

    // Update the actions from the tab widget.
    connect(tabWidgetPointer, SIGNAL(easyListStatusChanged(bool)), easyListActionPointer, SLOT(setChecked(bool)));
    connect(tabWidgetPointer, SIGNAL(easyPrivacyStatusChanged(bool)), easyPrivacyActionPointer, SLOT(setChecked(bool)));
    connect(tabWidgetPointer, SIGNAL(fanboysAnnoyanceListStatusChanged(bool)), fanboysAnnoyanceListPointer, SLOT(setChecked(bool)));
    connect(tabWidgetPointer, SIGNAL(ultraListStatusChanged(bool)), ultraListActionPointer, SLOT(setChecked(bool)));
    connect(tabWidgetPointer, SIGNAL(ultraPrivacyStatusChanged(bool)), ultraPrivacyActionPointer, SLOT(setChecked(bool)));
    connect(tabWidgetPointer, SIGNAL(updateDeveloperToolsAction(bool)), developerToolsActionPointer, SLOT(setChecked(bool)));
    connect(tabWidgetPointer, SIGNAL(updateBackAction(bool)), backActionPointer, SLOT(setEnabled(bool)));
    connect(tabWidgetPointer, SIGNAL(updateForwardAction(bool)), forwardActionPointer, SLOT(setEnabled(bool)));
    connect(tabWidgetPointer, SIGNAL(updateJavaScriptAction(bool)), this, SLOT(updateJavaScriptAction(bool)));
    connect(tabWidgetPointer, SIGNAL(updateLocalStorageAction(bool)), this, SLOT(updateLocalStorageAction(bool)));
    connect(tabWidgetPointer, SIGNAL(updateDomStorageAction(bool)), this, SLOT(updateDomStorageAction(bool)));

    // Connect the find text actions.
    connect(findCaseSensitiveActionPointer, SIGNAL(triggered()), this, SLOT(toggleFindCaseSensitive()));
    connect(hideFindTextActionPointer, SIGNAL(triggered()), this, SLOT(hideFindTextActions()));



    // Setup the GUI based on the browserwindowui.rc file.
    setupGUI(StandardWindowOption::Default, (QLatin1String("browserwindowui.rc")));

    // Set the open with other browser actions visibility.
    openWithFirefoxActionPointer->setVisible(globalFirefoxInstalled);
    openWithChromiumActionPointer->setVisible(globalChromiumInstalled);

    // Get lists of the actions' associated the widgets.
    QList<QObject*> userAgentAssociatedObjectsPointerList = userAgentPrivacyBrowserActionPointer->associatedObjects();
    QList<QObject*> searchEngineAssociatedObjectsPointerList = searchEngineMojeekActionPointer->associatedObjects();

    // Get the menu widget pointers.  It is the second entry, after the main window.
    QObject *userAgentMenuObjectPointer = userAgentAssociatedObjectsPointerList[1];
    QObject *searchEngineMenuObjectPointer = searchEngineAssociatedObjectsPointerList[1];

    // Get the menu pointers.
    QMenu *userAgentMenuPointer = qobject_cast<QMenu*>(userAgentMenuObjectPointer);
    QMenu *searchEngineMenuPointer = qobject_cast<QMenu*>(searchEngineMenuObjectPointer);

    // Get the menu actions.
    userAgentMenuActionPointer = userAgentMenuPointer->menuAction();
    searchEngineMenuActionPointer = searchEngineMenuPointer->menuAction();

    // Get handles for the toolbars.
    navigationToolBarPointer = toolBar(QLatin1String("navigation_toolbar"));
    urlToolBarPointer = toolBar(QLatin1String("url_toolbar"));
    bookmarksToolBarPointer = toolBar(QLatin1String("bookmarks_toolbar"));

    // Set the blocked requests action pointer widget object name (required to change the background color with a style sheet).
    urlToolBarPointer->widgetForAction(blockedRequestsActionPointer)->setObjectName("blocked_requests");

    // Populate the view bookmarks toolbar checkbox.
    connect(bookmarksToolBarPointer, SIGNAL(visibilityChanged(bool)), this, SLOT(updateViewBookmarksToolBarCheckbox(bool)));

    // Create the line edits.
    urlLineEditPointer = new UrlLineEdit();
    findTextLineEditPointer = new KLineEdit();

    // Get the line edit size policies.
    QSizePolicy urlLineEditSizePolicy = urlLineEditPointer->sizePolicy();
    QSizePolicy findTextLineEditSizePolicy = findTextLineEditPointer->sizePolicy();

    // Set the URL line edit horizontal stretch to be five times the find text line edit stretch.
    urlLineEditSizePolicy.setHorizontalStretch(5);
    findTextLineEditSizePolicy.setHorizontalStretch(1);

    // Set the policies.
    urlLineEditPointer->setSizePolicy(urlLineEditSizePolicy);
    findTextLineEditPointer->setSizePolicy(findTextLineEditSizePolicy);

    // Set the widths.
    urlLineEditPointer->setMinimumWidth(350);
    findTextLineEditPointer->setMinimumWidth(200);
    findTextLineEditPointer->setMaximumWidth(350);

    // Set the place holder text.
    urlLineEditPointer->setPlaceholderText(i18nc("The URL line edit placeholder text", "URL or Search Terms"));
    findTextLineEditPointer->setPlaceholderText(i18nc("The find line edit placeholder text", "Find Text"));

    // Show the clear button on the find line edit.
    findTextLineEditPointer->setClearButtonEnabled(true);

    // Add the actions to the URL line edit.
    bookmarkedActionPointer = urlLineEditPointer->addAction(QIcon::fromTheme(QLatin1String("non-starred-symbolic")), QLineEdit::LeadingPosition);
    QAction *addOrEditDomainSettingsActionPointer = urlLineEditPointer->addAction(QIcon::fromTheme(QLatin1String("settings-configure"), QIcon::fromTheme(QLatin1String("preferences-desktop"))),
                                                                                  QLineEdit::TrailingPosition);
    restoreUrlTextActionPointer = urlLineEditPointer->addAction(QIcon::fromTheme(QLatin1String("edit-undo")), QLineEdit::TrailingPosition);

    // Set the bookmarked action pointer to be checkable.
    bookmarkedActionPointer->setCheckable(true);

    // Connect the URL line edit actions.
    connect(bookmarkedActionPointer, SIGNAL(triggered()), this, SLOT(toggleBookmark()));
    connect(addOrEditDomainSettingsActionPointer, SIGNAL(triggered()), this, SLOT(addOrEditDomainSettings()));
    connect(restoreUrlTextActionPointer, SIGNAL(triggered()), this, SLOT(restoreUrlText()));

    // Create a find text label pointer.
    findTextLabelPointer = new QLabel();

    // Set the default label text.
    findTextLabelPointer->setText(QLatin1String("  ") + i18nc("Default find results.", "0/0") + QLatin1String("  "));

    // Insert the widgets into the toolbars.
    urlToolBarPointer->insertWidget(blockedRequestsActionPointer, urlLineEditPointer);
    findTextLineEditActionPointer = urlToolBarPointer->insertWidget(findNextActionPointer, findTextLineEditPointer);
    findTextLabelActionPointer = urlToolBarPointer->insertWidget(findNextActionPointer, findTextLabelPointer);

    // Initially hide the find text actions.
    hideFindTextActions();

    // Load a new URL from the URL line edit.
    connect(urlLineEditPointer, SIGNAL(returnKeyPressed(const QString)), this, SLOT(loadUrlFromLineEdit(const QString)));

    // Find text as it is typed.
    connect(findTextLineEditPointer, SIGNAL(textEdited(const QString &)), tabWidgetPointer, SLOT(findText(const QString &)));

    // Find next if the enter key is pressed.
    connect(findTextLineEditPointer, SIGNAL(returnKeyPressed(const QString &)), tabWidgetPointer, SLOT(findText(const QString &)));

    // Update find text when switching tabs.
    connect(tabWidgetPointer, SIGNAL(updateFindText(const QString &, const bool)), this, SLOT(updateFindText(const QString &, const bool)));

    // Update the find text results.
    connect(tabWidgetPointer, SIGNAL(updateFindTextResults(const QWebEngineFindTextResult &)), this, SLOT(updateFindTextResults(const QWebEngineFindTextResult &)));

    // Update the URL line edit on page loads.
    connect(tabWidgetPointer, SIGNAL(updateUrlLineEdit(QUrl)), this, SLOT(updateUrlLineEdit(QUrl)));

    // Update the window title.
    connect(tabWidgetPointer, SIGNAL(updateWindowTitle(const QString)), this, SLOT(updateWindowTitle(const QString)));

    // Store the URL line edit when it changes.
    connect(urlLineEditPointer, SIGNAL(textChanged(const QString &)), this, SLOT(processUrlTextChanges(const QString &)));

    // Get a handle for the status bar.
    QStatusBar *statusBarPointer = statusBar();

    // Create the status bar widgets.
    progressBarPointer = new QProgressBar();
    zoomMinusButtonPointer = new QPushButton();
    currentZoomButtonPointer = new QPushButton();
    zoomPlusButtonPointer = new QPushButton();

    // Set the button icons.
    zoomMinusButtonPointer->setIcon(QIcon::fromTheme(QLatin1String("zoom-out"), QIcon::fromTheme(QLatin1String("list-remove-symbolic"))));
    zoomPlusButtonPointer->setIcon(QIcon::fromTheme(QLatin1String("zoom-in"), QIcon::fromTheme(QLatin1String("list-add-symbolic"))));

    // Set the button icons to be flat (no borders).
    zoomMinusButtonPointer->setFlat(true);
    currentZoomButtonPointer->setFlat(true);
    zoomPlusButtonPointer->setFlat(true);

    // Handle clicks on the zoom buttons.
    connect(zoomMinusButtonPointer, SIGNAL(clicked()), this, SLOT(decrementZoom()));
    connect(currentZoomButtonPointer, SIGNAL(clicked()), this, SLOT(getZoomFactorFromUser()));
    connect(zoomPlusButtonPointer, SIGNAL(clicked()), this, SLOT(incrementZoom()));

    // Remove the padding around the current zoom button text.
    currentZoomButtonPointer->setStyleSheet(QLatin1String("padding: 0px;"));

    // Add the widgets to the far right of the status bar.
    statusBarPointer->addPermanentWidget(progressBarPointer);
    statusBarPointer->addPermanentWidget(zoomMinusButtonPointer);
    statusBarPointer->addPermanentWidget(currentZoomButtonPointer);
    statusBarPointer->addPermanentWidget(zoomPlusButtonPointer);

    // Update the status bar with the URL when a link is hovered.
    connect(tabWidgetPointer, SIGNAL(linkHovered(QString)), statusBarPointer, SLOT(showMessage(QString)));

    // Update the progress bar.
    connect(tabWidgetPointer, SIGNAL(showProgressBar(const int)), this, SLOT(showProgressBar(const int)));
    connect(tabWidgetPointer, SIGNAL(hideProgressBar()), this, SLOT(hideProgressBar()));

    // Update the URL line edit focus.
    connect(tabWidgetPointer, SIGNAL(clearUrlLineEditFocus()), this, SLOT(clearUrlLineEditFocus()));

    // Get the URL line edit palettes.
    normalBackgroundPalette = urlLineEditPointer->palette();
    negativeBackgroundPalette = normalBackgroundPalette;
    positiveBackgroundPalette = normalBackgroundPalette;

    // Modify the palettes.
    KColorScheme::adjustBackground(negativeBackgroundPalette, KColorScheme::NegativeBackground);
    KColorScheme::adjustBackground(positiveBackgroundPalette, KColorScheme::PositiveBackground);

    // Update the applied palette.
    connect(tabWidgetPointer, SIGNAL(updateDomainSettingsIndicator(const bool)), this, SLOT(updateDomainSettingsIndicator(const bool)));

    // Process full screen requests.
    connect(tabWidgetPointer, SIGNAL(fullScreenRequested(bool)), this, SLOT(fullScreenRequested(bool)));

    // Create keyboard shortcuts.
    QShortcut *f11ShortcutPointer = new QShortcut(QKeySequence(i18nc("The toggle full screen shortcut.", "F11")), this);
    QShortcut *ctrlShiftRShortcut = new QShortcut(QKeySequence(i18nc("The restore URL text key sequence.", "Ctrl+Shift+R")), this);
    QShortcut *escapeShortcutPointer = new QShortcut(QKeySequence::Cancel, this);

    // Connect the keyboard shortcuts.
    connect(f11ShortcutPointer, SIGNAL(activated()), fullScreenActionPointer, SLOT(trigger()));
    connect(ctrlShiftRShortcut, SIGNAL(activated()), restoreUrlTextActionPointer, SLOT(trigger()));
    connect(escapeShortcutPointer, SIGNAL(activated()), this, SLOT(escape()));

    // Get a handle for the Bookmarks menu.
    bookmarksMenuPointer = qobject_cast<QMenu*>(guiFactory()->container(QLatin1String("bookmarks"), this));

    // Add a separator to the bookmarks menu.
    bookmarksMenuPointer->addSeparator();

    // Initialize the bookmark action lists.
    bookmarkFolderFinalActionList = QList<QPair<QMenu *, QAction *> *>();
    bookmarksMenuActionList = QList<QPair<QMenu *, QAction *> *>();
    bookmarksMenuSubmenuList = QList<QPair<QMenu *, QMenu *> *>();
    bookmarksToolBarActionList = QList<QAction*>();
    bookmarksToolBarSubfolderActionList = QList<QPair<QMenu *, QAction *> *>();

    // Set the bookmarks toolbar context menu policy.
    bookmarksToolBarPointer->setContextMenuPolicy(Qt::CustomContextMenu);

    // Show the custom bookmark context menu when requested.
    connect(bookmarksToolBarPointer, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(showBookmarkContextMenu(const QPoint&)));

    // Populate the bookmarks in this window.
    populateBookmarksInThisWindow();

    // Populate the UI.
    // This must be done here, because otherwise, if a URL is loaded, like a local file, that does not trigger a call to TabWidget::applyDomainSettings, the UI will not be fully populated.
    updateJavaScriptAction(Settings::javaScriptEnabled());
    updateLocalStorageAction(Settings::localStorageEnabled());
    updateDomStorageAction(Settings::domStorageEnabled());
    updateUserAgentActions(UserAgentHelper::getUserAgentFromDatabaseName(Settings::userAgent()), true);
    updateZoomActions(Settings::zoomFactor());

    // Populate the first tab.
    if (firstWindow)  // This is the first window.
    {
        // Load the initial website.
        tabWidgetPointer->loadInitialWebsite();
    }
    else if (initialUrlStringPointer)  // An initial URL was specified.
    {
        // Load the initial URL.
        tabWidgetPointer->loadUrlFromLineEdit(*initialUrlStringPointer);
    }
}

// If actions are part of a context menu they do not need to be added to the list as they will be deleted automatically when the context menu closes.
void BrowserWindow::addBookmarkFolderFinalActions(QMenu *menuPointer, const double folderId, const bool addToList)
{
    // Get the database ID.
    int folderDatabaseId = BookmarksDatabase::getFolderDatabaseId(folderId);

    // Add a separator.
    menuPointer->addSeparator();

    // Add the add bookmark action to the menu.
    QAction *addBookmarkActionPointer = menuPointer->addAction(QIcon::fromTheme(QLatin1String("bookmark-new")), i18nc("The add bookmark action", "Add Bookmark"), [=]
        {
            // Instantiate an add bookmark dialog.
            AddBookmarkDialog *addBookmarkDialogPointer = new AddBookmarkDialog(this, tabWidgetPointer->getCurrentTabTitle(), tabWidgetPointer->getCurrentTabUrl(),
                                                                                tabWidgetPointer->getCurrentTabFavoritIcon(), folderId);

            // Update the displayed bookmarks when a new one is added.
            connect(addBookmarkDialogPointer, SIGNAL(bookmarkAdded()), this, SLOT(populateBookmarksInAllWindows()));

            // Show the dialog.
            addBookmarkDialogPointer->show();
        }
    );

    // Add the add folder action to the menu.
    QAction *addFolderActionPointer = menuPointer->addAction(QIcon::fromTheme(QLatin1String("folder-add")), i18nc("The add folder action", "Add Folder"), [=]
        {
            // Instantiate an add folder dialog.
            AddFolderDialog *addFolderDialogPointer = new AddFolderDialog(this, tabWidgetPointer->getCurrentTabFavoritIcon(), folderId);

            // Update the displayed bookmarks when a folder is added.
            connect(addFolderDialogPointer, SIGNAL(folderAdded()), this, SLOT(populateBookmarksInAllWindows()));

            // Show the dialog.
            addFolderDialogPointer->show();
        }
    );

    // Add a separator.
    menuPointer->addSeparator();

    // Add the open folder in new tabs action to the menu.
    QAction *openFolderInNewTabsActionPointer = menuPointer->addAction(QIcon::fromTheme(QLatin1String("tab-new")), i18nc("The open folder in new tabs action", "Open Folder in New Tabs"), [=]
        {
            // Get all the folder URLs.
            QList<QString> *folderUrlsListPointer = BookmarksDatabase::getAllFolderUrls(folderId);

            // Open the URLs in new tabs.  `true` removes the URL line edit focus, `true` opens the new tabs in an adjacent tab.  `false` does not load a background tab.
            for (QString url : *folderUrlsListPointer)
                tabWidgetPointer->addTab(true, true, false, url);
        }
    );

    // Add the open folder in background tabs action to the menu.
    QAction *openFolderInBackgroundTabsActionPointer = menuPointer->addAction(QIcon::fromTheme(QLatin1String("tab-new")),
                                                                              i18nc("The open folder in background tabs action", "Open Folder in Background Tabs"), [=]
        {
            // Get all the folder URLs.
            QList<QString> *folderUrlsListPointer = BookmarksDatabase::getAllFolderUrls(folderId);

            // Open the URLs in new tabs.  `true` removes the URL line edit focus, `true` opens the new tabs in an adjacent tab.  `true` loads a background tab.
            for (QString url : *folderUrlsListPointer)
                tabWidgetPointer->addTab(true, true, true, url);
        }
    );

    // Add the open folder in new window action to the menu.
    QAction *openFolderInNewWindowActionPointer = menuPointer->addAction(QIcon::fromTheme(QLatin1String("window-new")), i18nc("The open folder in new window action", "Open Folder in New Window"), [=]
        {
            // Get all the folder URLs.
            QList<QString> *folderUrlsListPointer = BookmarksDatabase::getAllFolderUrls(folderId);

            // Create a new browser window.
            BrowserWindow *browserWindowPointer = new BrowserWindow(false, &folderUrlsListPointer->first());

            // Get a count of the folder URLs.
            const int folderUrls = folderUrlsListPointer->count();

            // Load all the other URLs.  `true` removes the URL line edit focus, `false` does not load the new tabs in adjacent tabs.  `true` loads a background tab.
            for (int i = 1; i < folderUrls; ++i)
                browserWindowPointer->tabWidgetPointer->addTab(true, false, true, folderUrlsListPointer->value(i));

            // Show the new browser window.
            browserWindowPointer->show();
        }
    );

    // Add a separator.
    menuPointer->addSeparator();

    // Add the edit folder action to the menu if this is not the root bookmark menu.
    if (folderId != 0)
    {
        QAction *editFolderActionPointer = menuPointer->addAction(QIcon::fromTheme(QLatin1String("document-edit")), i18nc("The edit folder action", "Edit Folder"), [=]
            {
                // Get the current tab favorite icon.
                QIcon currentTabFavoriteIcon = tabWidgetPointer->getCurrentTabFavoritIcon();

                // Instantiate an edit folder dialog.
                QDialog *editFolderDialogPointer = new EditFolderDialog(this, folderDatabaseId, currentTabFavoriteIcon);

                // Show the dialog.
                editFolderDialogPointer->show();

                // Process bookmark events.
                connect(editFolderDialogPointer, SIGNAL(folderSaved()), this, SLOT(populateBookmarksInAllWindows()));
            }
        );

        // Add the action to the beginning of the bookmark folder final action list if requsted.
        if (addToList)
            bookmarkFolderFinalActionList.prepend(new QPair<QMenu *, QAction *>(menuPointer, editFolderActionPointer));
    }

    // Add the delete folder action to the menu.
    QAction *deleteFolderActionPointer = menuPointer->addAction(QIcon::fromTheme(QLatin1String("edit-delete")), i18nc("Delete folder context menu entry", "Delete Folder"), [=]
        {
            // Create an items to delete list.
            QList<int>* itemsToDeleteListPointer = new QList<int>;

            // Add the folder to the list of items to delete if it is not the root folder.
            if (folderId != 0)
                itemsToDeleteListPointer->append(folderDatabaseId);

            // Add the folder contents to the list of items to delete.
            itemsToDeleteListPointer->append(*BookmarksDatabase::getFolderContentsDatabaseIdsRecursively(folderId));

            // Instantiate a delete dialog message box.
            QMessageBox deleteDialogMessageBox;

            // Set the icon.
            deleteDialogMessageBox.setIcon(QMessageBox::Warning);

            // Set the window title.
            deleteDialogMessageBox.setWindowTitle(i18nc("Delete bookmarks dialog title", "Delete Bookmarks"));

            // Set the text.
            deleteDialogMessageBox.setText(i18ncp("Delete bookmarks dialog main message", "Delete %1 bookmark item?", "Delete %1 bookmark items?", itemsToDeleteListPointer->count()));

            // Set the informative text.
            deleteDialogMessageBox.setInformativeText(i18nc("Delete bookmarks dialog secondary message", "This cannot be undone."));

            // Set the standard buttons.
            deleteDialogMessageBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);

            // Set the default button.
            deleteDialogMessageBox.setDefaultButton(QMessageBox::No);

            // Display the dialog and capture the return value.
            int returnValue = deleteDialogMessageBox.exec();

            // Delete the domain if instructed.
            if (returnValue == QMessageBox::Yes)
            {
                // Get the parent folder ID.
                double parentFolderId = BookmarksDatabase::getParentFolderId(folderDatabaseId);

                // Delete the folder and its contents.
                for (const int databaseId : *itemsToDeleteListPointer)
                    BookmarksDatabase::deleteBookmark(databaseId);

                // Update the display order of the bookmarks in the parent folder.
                BookmarksDatabase::updateFolderContentsDisplayOrder(parentFolderId);

                // Repopulate the bookmarks.
                populateBookmarksInAllWindows();
            }
        }
    );

    // Add the key sequences if this is the root bookmarks menu.
    if (folderId == 0)
    {
        // Create the key sequences.
        QKeySequence ctrlBKeySequence = QKeySequence(i18nc("The add bookmark key sequence.", "Ctrl+B"));
        QKeySequence metaFKeySequence = QKeySequence(i18nc("The add folder key sequence.", "Meta+F"));

        // Set the action key sequences.
        actionCollectionPointer->setDefaultShortcut(addBookmarkActionPointer, ctrlBKeySequence);
        actionCollectionPointer->setDefaultShortcut(addFolderActionPointer, metaFKeySequence);
    }

    // Add the actions to the beginning of the bookmark folder final action list if requested.
    if (addToList) {
        bookmarkFolderFinalActionList.prepend(new QPair<QMenu *, QAction *>(menuPointer, addBookmarkActionPointer));
        bookmarkFolderFinalActionList.prepend(new QPair<QMenu *, QAction *>(menuPointer, addFolderActionPointer));
        bookmarkFolderFinalActionList.prepend(new QPair<QMenu *, QAction *>(menuPointer, openFolderInNewTabsActionPointer));
        bookmarkFolderFinalActionList.prepend(new QPair<QMenu *, QAction *>(menuPointer, openFolderInBackgroundTabsActionPointer));
        bookmarkFolderFinalActionList.prepend(new QPair<QMenu *, QAction *>(menuPointer, openFolderInNewWindowActionPointer));
        bookmarkFolderFinalActionList.prepend(new QPair<QMenu *, QAction *>(menuPointer, deleteFolderActionPointer));
    }
}

void BrowserWindow::addOrEditDomainSettings()
{
    // Remove the focus from the URL line edit.
    urlLineEditPointer->clearFocus();

    // Get the current domain settings name.
    QString currentDomainSettingsName = tabWidgetPointer->getDomainSettingsName();

    // Run the commands according to the current domain settings status.
    if (currentDomainSettingsName == QStringLiteral(""))  // Domain settings are not currently applied.
    {
        // Get the current settings status.
        int javaScriptInt = calculateSettingsInt(javaScriptEnabled, Settings::javaScriptEnabled());
        int localStorageInt = calculateSettingsInt(localStorageActionPointer->isChecked(), Settings::localStorageEnabled());
        int domStorageInt = calculateSettingsInt(domStorageActionPointer->isChecked(), Settings::domStorageEnabled());
        int ultraPrivacyInt = calculateSettingsInt(ultraPrivacyActionPointer->isChecked(), Settings::ultraPrivacyEnabled());
        int ultraListInt = calculateSettingsInt(ultraListActionPointer->isChecked(), Settings::ultraListEnabled());
        int easyPrivacyInt = calculateSettingsInt(easyPrivacyActionPointer->isChecked(), Settings::easyPrivacyEnabled());
        int easyListInt = calculateSettingsInt(easyListActionPointer->isChecked(), Settings::easyListEnabled());
        int fanboysAnnoyanceListInt = calculateSettingsInt(fanboysAnnoyanceListPointer->isChecked(), Settings::fanboysAnnoyanceListEnabled());

        // Get the current user agent string.
        QString currentUserAgentString = tabWidgetPointer->getCurrentUserAgent();

        // Get the current user agent database string.
        QString currentUserAgentDatabaseString = UserAgentHelper::getDatabaseUserAgentNameFromUserAgent(currentUserAgentString);

        // Initialize the user agent database string.
        QString userAgentDatabaseString = UserAgentHelper::SYSTEM_DEFAULT_DATABASE;

        // Replace the user agent database string if the current user agent is not the default.
        if (currentUserAgentDatabaseString != Settings::userAgent())
            userAgentDatabaseString = currentUserAgentDatabaseString;

        // Initialize the zoom factor variables.
        int zoomFactorInt = DomainsDatabase::SYSTEM_DEFAULT;

        // Use a custom zoom factor if currently applied.  Doubles cannot be reliably compared using `==`, so a mathematical workaround is used.
        if (abs(currentZoomFactorDouble - defaultZoomFactorDouble ) > 0.01)
            zoomFactorInt = DomainsDatabase::CUSTOM;

        // Update the current domain settings name.
        currentDomainSettingsName = currentUrl.host();

        // Add the domain.
        DomainsDatabase::addDomain(currentDomainSettingsName, javaScriptInt, localStorageInt, domStorageInt, userAgentDatabaseString, ultraPrivacyInt, ultraListInt, easyPrivacyInt, easyListInt,
                                   fanboysAnnoyanceListInt, zoomFactorInt, currentZoomFactorDouble);

        // Apply the domain settings in all windows.
        applyDomainSettingsInAllWindows();
    }

    // Create the domain settings dialog pointer.
    DomainSettingsDialog *domainSettingsDialogPointer = new DomainSettingsDialog(this, DomainSettingsDialog::EDIT_DOMAIN, currentDomainSettingsName);

    // Apply the domain settings in all windows when domain settings are updated.
    connect(domainSettingsDialogPointer, SIGNAL(domainSettingsUpdated()), this, SLOT(applyDomainSettingsInAllWindows()));

    // Show the dialog.
    domainSettingsDialogPointer->show();
}

void BrowserWindow::applyDomainSettingsInAllWindows() const
{
    // Apply the domain settings in all windows.
    runMethodInAllWindows(QLatin1String("applyDomainSettingsInThisWindow"));
}

void BrowserWindow::applyDomainSettingsInThisWindow()
{
    // Apply domain settings and reload the tabs.
    tabWidgetPointer->applyDomainSettingsAndReload();
}

void BrowserWindow::back() const
{
    // Remove the focus from the URL line edit.
    urlLineEditPointer->clearFocus();

    // Go back.
    tabWidgetPointer->back();
}

int BrowserWindow::calculateSettingsInt(const bool settingCurrentlyEnabled, const bool settingEnabledByDefault) const
{
    // Return the int that matches the current state.
    if (settingCurrentlyEnabled == settingEnabledByDefault)  // The current system default is used.
        return DomainsDatabase::SYSTEM_DEFAULT;
    else if (settingCurrentlyEnabled)  // The setting is enabled, which is different from the system default.
        return DomainsDatabase::ENABLED;
    else  // The setting is disabled, which is different from the system default.
        return DomainsDatabase::DISABLED;
}

void BrowserWindow::clearUrlLineEditFocus() const
{
    // Remove the focus from the URL line edit.
    urlLineEditPointer->clearFocus();
}

void BrowserWindow::decrementZoom()
{
    // Update the current zoom factor.
    currentZoomFactorDouble = currentZoomFactorDouble - 0.25;

    // Check to make sure the zoom factor is in the valid range (0.25 to 5.00).
    if (currentZoomFactorDouble < 0.25)
        currentZoomFactorDouble = 0.25;

    // Set the new zoom factor.
    tabWidgetPointer->applyOnTheFlyZoomFactor(currentZoomFactorDouble);

    // Update the on-the-fly action text.
    updateZoomActions(currentZoomFactorDouble);
}

void BrowserWindow::editBookmarks()
{
    // Instantiate an edit bookmarks dialog.
    BookmarksDialog *bookmarksDialogPointer = new BookmarksDialog(this, tabWidgetPointer->getCurrentTabFavoritIcon(), tabWidgetPointer->getCurrentTabTitle(), tabWidgetPointer->getCurrentTabUrl());

    // Update the displayed bookmarks when edited.
    connect(bookmarksDialogPointer, SIGNAL(bookmarkUpdated()), this, SLOT(populateBookmarksInAllWindows()));

    // Show the dialog.
    bookmarksDialogPointer->show();
}

void BrowserWindow::escape() const
{
    // Process the escape according to the status of the browser.
    if (fullScreenActionPointer->isChecked())  // Full screen browsing is enabled.
    {
        // Exit full screen browsing.
        fullScreenActionPointer->trigger();
    }
    else if (!findTextLineEditPointer->text().isEmpty())  // Find text is populated.
    {
        // Clear the find text line edit.
        findTextLineEditPointer->clear();

        // Clear the search in the WebEngine.
        tabWidgetPointer->findText(QStringLiteral(""));
    }
    else if (findTextLineEditActionPointer->isVisible())  // Find text actions are visible.
    {
        // Hide the find text actions.
        hideFindTextActions();
    }
}

void BrowserWindow::findNext() const
{
    // Get the find string.
    const QString findString = findTextLineEditPointer->text();

    // Search for the text if it is not empty.
    if (!findString.isEmpty())
        tabWidgetPointer->findText(findString);
}

void BrowserWindow::findPrevious() const
{
    // Get the find string.
    const QString findString = findTextLineEditPointer->text();

    // Search for the text if it is not empty.
    if (!findString.isEmpty())
        tabWidgetPointer->findPrevious(findString);
}

void BrowserWindow::forward() const
{
    // Remove the focus from the URL line edit.
    urlLineEditPointer->clearFocus();

    // Go forward.
    tabWidgetPointer->forward();
}

void BrowserWindow::fullScreenRequested(const bool toggleOn)
{
    // Toggle full screen mode.
    if (toggleOn)  // Turn full screen mode on.
    {
        // Enable full screen mode.
        fullScreenActionPointer->setFullScreen(window(), true);

        // Hide the menu bar if specified.
        if (Settings::fullScreenHideMenuBar())
            menuBar()->setVisible(false);

        // Hide the toolbars if specified.
        if (Settings::fullScreenHideToolBars())
        {
            navigationToolBarPointer->setVisible(false);
            urlToolBarPointer->setVisible(false);
            bookmarksToolBarPointer->setVisible(false);
        }

        // Hide the tab bar if specified.
        if (Settings::fullScreenHideTabBar())
            tabWidgetPointer->setTabBarVisible(false);

        // Hide the status bar if specified.
        if (Settings::fullScreenHideStatusBar())
            statusBar()->setVisible(false);
    }
    else  // Disable full screen browsing mode.
    {
        // Disable full screen mode.
        fullScreenActionPointer->setFullScreen(window(), false);

        // Show the menu bar.
        menuBar()->setVisible(true);

        // Show the toolbars.
        navigationToolBarPointer->setVisible(true);
        urlToolBarPointer->setVisible(true);

        // Only show the bookmarks toolbar if it was previously visible.
        if (bookmarksToolBarIsVisible)
            bookmarksToolBarPointer->setVisible(true);

        // Show the tab bar.
        tabWidgetPointer->setTabBarVisible(true);

        // Show the status bar.
        statusBar()->setVisible(true);
    }
}

void BrowserWindow::getZoomFactorFromUser()
{
    // Create an OK flag.
    bool okClicked;

    // Display a dialog to get the new zoom factor from the user.  Format the double to display two decimals and have a 0.25 step.
    double newZoomFactorDouble = QInputDialog::getDouble(this, i18nc("The on-the-fly zoom factor dialog title", "On-The-Fly Zoom Factor"),
                                                   i18nc("The instruction text of the on-the-fly zoom factor dialog", "Enter a zoom factor between 0.25 and 5.00"),
                                                   currentZoomFactorDouble, .025, 5.00, 2, &okClicked, Qt::WindowFlags(), 0.25);

    // Update the zoom factor if the user clicked OK.
    if (okClicked)
    {
        // Set the new zoom factor.
        tabWidgetPointer->applyOnTheFlyZoomFactor(newZoomFactorDouble);

        // Update the on-the-fly action text.
        updateZoomActions(newZoomFactorDouble);
    }
}

void BrowserWindow::hideFindTextActions() const
{
    // Hide the find text actions.
    findTextLineEditActionPointer->setVisible(false);
    findTextLabelActionPointer->setVisible(false);
    findNextActionPointer->setVisible(false);
    findPreviousActionPointer->setVisible(false);
    findCaseSensitiveActionPointer->setVisible(false);
    hideFindTextActionPointer->setVisible(false);
}

void BrowserWindow::home() const
{
    // Remove the focus from the URL line edit.
    urlLineEditPointer->clearFocus();

    // Go home.
    tabWidgetPointer->home();
}

void BrowserWindow::hideProgressBar() const
{
    // Hide the progress bar.
    progressBarPointer->hide();

    // Disable and hide the stop action.
    stopActionPointer->setEnabled(false);
    stopActionPointer->setVisible(false);

    // Enable and show the refresh action.
    refreshActionPointer->setEnabled(true);
    refreshActionPointer->setVisible(true);
}

void BrowserWindow::incrementZoom()
{
    // Update the current zoom factor.
    currentZoomFactorDouble = currentZoomFactorDouble + 0.25;

    // Check to make sure the zoom factor is in the valid range (0.25 to 5.00).
    if (currentZoomFactorDouble > 5.0)
        currentZoomFactorDouble = 5.0;

    // Set the new zoom factor.
    tabWidgetPointer->applyOnTheFlyZoomFactor(currentZoomFactorDouble);

    // Update the on-the-fly action text.
    updateZoomActions(currentZoomFactorDouble);
}

void BrowserWindow::loadUrlFromLineEdit(const QString &url) const
{
    // Remove the focus from the URL line edit.
    urlLineEditPointer->clearFocus();

    // Load the URL.
    tabWidgetPointer->loadUrlFromLineEdit(url);
}

void BrowserWindow::newWindow() const
{
    // Create a new browser window.
    BrowserWindow *browserWindowPointer = new BrowserWindow();

    // Show the new browser window.
    browserWindowPointer->show();
}

void BrowserWindow::openWithChromium() const
{
    // Open the current URL in Chromium
    QProcess::startDetached(QLatin1String("chromium"), QStringList(urlLineEditPointer->text()));
}

void BrowserWindow::openWithFirefox() const
{
    // Open the current URL in Firefox.
    QProcess::startDetached(QLatin1String("firefox-esr"), QStringList(urlLineEditPointer->text()));
}

void BrowserWindow::populateBookmarksInAllWindows() const
{
    // Populate the bookmarks in all windows.
    runMethodInAllWindows(QLatin1String("populateBookmarksInThisWindow"));
}

void BrowserWindow::populateBookmarksInThisWindow()
{
    // Remove all the final bookmark folder menu actions.
    for (QPair<QMenu *, QAction *> *bookmarkFolderFinalActionPairPointer : bookmarkFolderFinalActionList)
    {
        // Remove the action.
        bookmarkFolderFinalActionPairPointer->first->removeAction(bookmarkFolderFinalActionPairPointer->second);
    }

    // Remove all the current menu bookmarks.
    for (QPair<QMenu *, QAction *> *bookmarkMenuActionPairPointer : bookmarksMenuActionList)
    {
        // Remove the bookmark.
        bookmarkMenuActionPairPointer->first->removeAction(bookmarkMenuActionPairPointer->second);
    }

    // Remove all the current menu subfolders.
    for (QPair<QMenu *, QMenu *> *submenuPairPointer : bookmarksMenuSubmenuList)
    {
        // Remove the submenu from the parent menu.
        submenuPairPointer->first->removeAction(submenuPairPointer->second->menuAction());
    }

    // Remove all the current toolbar subfolders.
    for (QPair<QMenu *, QAction *> *subfolderActionPairPointer : bookmarksToolBarSubfolderActionList)
    {
        // Remove the action from the subfolder.
        subfolderActionPairPointer->first->removeAction(subfolderActionPairPointer->second);
    }

    // Remove all the current toolbar bookmarks.
    for (QAction *bookmarkAction : bookmarksToolBarActionList)
    {
        // Remove the bookmark.
        bookmarksToolBarPointer->removeAction(bookmarkAction);
    }

    // Clear the action lists.
    bookmarkFolderFinalActionList.clear();
    bookmarksMenuActionList.clear();
    bookmarksMenuSubmenuList.clear();
    bookmarksToolBarSubfolderActionList.clear();
    bookmarksToolBarActionList.clear();

    // Populate the bookmarks subfolders, beginning with the root folder (`0`);
    populateBookmarksMenuSubfolders(0, bookmarksMenuPointer);

    // Populate the bookmarks toolbar.
    populateBookmarksToolBar();

    // Get a handle for the bookmark toolbar layout.
    QLayout *bookmarksToolBarLayoutPointer = bookmarksToolBarPointer->layout();

    // Get the count of the bookmarks.
    int bookmarkCount = bookmarksToolBarLayoutPointer->count();

    // Set the layout of each bookmark to be left aligned.
    for(int i = 0; i < bookmarkCount; ++i)
        bookmarksToolBarLayoutPointer->itemAt(i)->setAlignment(Qt::AlignLeft);

    // Update the bookmarked action.
    updateBookmarkedAction();
}

void BrowserWindow::populateBookmarksMenuSubfolders(const double folderId, QMenu *menuPointer)
{
    // Get the folder contents.
    QList<BookmarkStruct> *folderContentsListPointer = BookmarksDatabase::getFolderContents(folderId);

    // Populate the bookmarks menu and toolbar.
    for (BookmarkStruct bookmarkStruct : *folderContentsListPointer)
    {
        // Process the item according to the type.
        if (bookmarkStruct.isFolder)  // This item is a folder.
        {
            // Add a submenu to the menu.
            QMenu *submenuPointer = menuPointer->addMenu(bookmarkStruct.favoriteIcon, bookmarkStruct.name);

            // Add the submenu to the beginning of the list of menus to be deleted on repopulate.
            bookmarksMenuSubmenuList.prepend(new QPair<QMenu *, QMenu *>(menuPointer, submenuPointer));

            // Populate any subfolders.
            populateBookmarksMenuSubfolders(bookmarkStruct.folderId, submenuPointer);
        }
        else  // This item is a bookmark.
        {
            // Add the bookmark to the menu.
            QAction *menuBookmarkActionPointer = menuPointer->addAction(bookmarkStruct.favoriteIcon, bookmarkStruct.name, [=]
                {
                    // Remove the focus from the URL line edit.
                    urlLineEditPointer->clearFocus();

                    // Load the URL.
                    tabWidgetPointer->loadUrlFromLineEdit(bookmarkStruct.url);
                }
            );

            // Add the actions to the beginning of the list of bookmarks to be deleted on repopulate.
            bookmarksMenuActionList.prepend(new QPair<QMenu *, QAction *>(menuPointer, menuBookmarkActionPointer));
        }
    }

    // Add the extra items at the bottom of the menu.  `true` adds them to the list of actions to be deleted on repopulate.
    addBookmarkFolderFinalActions(menuPointer, folderId, true);
}

void BrowserWindow::populateBookmarksToolBar()
{
    // Get the root folder contents (which has a folder ID of `0`).
    QList<BookmarkStruct> *folderContentsListPointer = BookmarksDatabase::getFolderContents(0);

    // Populate the bookmarks toolbar.
    for (BookmarkStruct bookmarkStruct : *folderContentsListPointer)
    {
        // Process the item according to the type.
        if (bookmarkStruct.isFolder)  // This item is a folder.
        {
            // Add the subfolder action.
            QAction *toolBarSubfolderActionPointer = bookmarksToolBarPointer->addAction(bookmarkStruct.favoriteIcon, bookmarkStruct.name);

            // Add the bookmark database ID to the toolbar action.
            toolBarSubfolderActionPointer->setData(bookmarkStruct.databaseId);

            // Add the action to the beginning of the list of actions to be deleted on repopulate.
            bookmarksToolBarActionList.prepend(toolBarSubfolderActionPointer);

            // Create a subfolder menu.
            QMenu *subfolderMenuPointer = new QMenu();

            // Add the menu to the action.
            toolBarSubfolderActionPointer->setMenu(subfolderMenuPointer);

            // Add the submenu to the toolbar menu list.
            bookmarksToolBarMenuList.append(new QPair<QMenu *, const double>(subfolderMenuPointer, bookmarkStruct.folderId));

            // Set the popup mode for the menu.
            dynamic_cast<QToolButton *>(bookmarksToolBarPointer->widgetForAction(toolBarSubfolderActionPointer))->setPopupMode(QToolButton::InstantPopup);

            // Populate the subfolder.
            populateBookmarksToolBarSubfolders(bookmarkStruct.folderId, subfolderMenuPointer);
        }
        else  // This item is a bookmark.
        {
            // Add the bookmark to the toolbar.
            QAction *toolBarBookmarkActionPointer = bookmarksToolBarPointer->addAction(bookmarkStruct.favoriteIcon, bookmarkStruct.name, [=]
                {
                    // Remove the focus from the URL line edit.
                    urlLineEditPointer->clearFocus();

                    // Load the URL.
                    tabWidgetPointer->loadUrlFromLineEdit(bookmarkStruct.url);
                }
            );

            // Add the bookmark database ID to the toolbar action.
            toolBarBookmarkActionPointer->setData(bookmarkStruct.databaseId);

            // Add the actions to the beginning of the current bookmarks lists.
            bookmarksToolBarActionList.prepend(toolBarBookmarkActionPointer);
        }
    }

    // Add the extra items to the toolbar folder menus.  The first item in the pair is the menu pointer.  The second is the folder ID.
    for (QPair<QMenu *, const double> *menuAndFolderIdPairPointer : bookmarksToolBarMenuList)
    {
        // Populate the final bookmarks menu entries.  `true` adds them to the list of actions to be deleted on repopulate.
        addBookmarkFolderFinalActions(menuAndFolderIdPairPointer->first, menuAndFolderIdPairPointer->second, true);
    }
}

void BrowserWindow::populateBookmarksToolBarSubfolders(const double folderId, QMenu *menuPointer)
{
    // Get the folder contents.
    QList<BookmarkStruct> *folderContentsListPointer = BookmarksDatabase::getFolderContents(folderId);

    // Populate the bookmarks folder.
    for (BookmarkStruct bookmarkStruct : *folderContentsListPointer)
    {
        // Get the bookmark URL.
        QString bookmarkUrl = bookmarkStruct.url;

        // Process the item according to the type.
        if (bookmarkStruct.isFolder)  // This item is a folder.
        {
            // Add the subfolder action.
            QAction *toolBarSubfolderActionPointer = menuPointer->addAction(bookmarkStruct.favoriteIcon, bookmarkStruct.name);

            // Add the action to the beginning of the list of actions to be deleted on repopulate.
            bookmarksToolBarSubfolderActionList.prepend(new QPair<QMenu *, QAction *>(menuPointer, toolBarSubfolderActionPointer));

            // Create a subfolder menu.
            QMenu *subfolderMenuPointer = new QMenu();

            // Add the submenu to the action.
            toolBarSubfolderActionPointer->setMenu(subfolderMenuPointer);

            // Add the submenu to the toolbar menu list.
            bookmarksToolBarMenuList.append(new QPair<QMenu *, const double>(subfolderMenuPointer, bookmarkStruct.folderId));

            // Populate the subfolder menu.
            populateBookmarksToolBarSubfolders(bookmarkStruct.folderId, subfolderMenuPointer);
        }
        else  // This item is a bookmark.
        {
            // Add the bookmark to the folder.
            QAction *toolBarBookmarkActionPointer = menuPointer->addAction(bookmarkStruct.favoriteIcon, bookmarkStruct.name, [=]
                {
                    // Remove the focus from the URL line edit.
                    urlLineEditPointer->clearFocus();

                    // Load the URL.
                    tabWidgetPointer->loadUrlFromLineEdit(bookmarkUrl);
                }
            );

            // Add the bookmark database ID to the toolbar action.
            toolBarBookmarkActionPointer->setData(bookmarkStruct.databaseId);

            // Add the action to the beginning of the list of actions to be deleted on repopulate.
            bookmarksToolBarSubfolderActionList.prepend(new QPair<QMenu *, QAction *>(menuPointer, toolBarBookmarkActionPointer));
        }
    }
}

void BrowserWindow::processUrlTextChanges(const QString &newUrlText) const
{
    // Update the visibility of the restore URL text action pointer.
    restoreUrlTextActionPointer->setVisible(newUrlText != tabWidgetPointer->getCurrentTabUrl());

    // Store the new URL text in the WebEngine View.
    tabWidgetPointer->storeCurrentUrlText(newUrlText);
}

void BrowserWindow::refresh() const
{
    // Remove the focus from the URL line edit.
    urlLineEditPointer->clearFocus();

    // Refresh the web page.
    tabWidgetPointer->refresh();
}

void BrowserWindow::reloadAndBypassCache() const
{
    // Remove the focus from the URL line edit.
    urlLineEditPointer->clearFocus();

    // Refresh the web page.
    tabWidgetPointer->refresh();
}

void BrowserWindow::restoreUrlText() const
{
    // Restore the URL text to the current URL.
    urlLineEditPointer->setText(tabWidgetPointer->getCurrentTabUrl());
}

void BrowserWindow::runMethodInAllWindows(const QString &methodString) const
{
    // Get a list of all the registered service names.
    QStringList registeredServiceNames = QDBusConnection::sessionBus().interface()->registeredServiceNames().value();

    // Get a list of all the Privacy Browser windows, which will be `com.stoutner.privacybrowser-` with the PID appended.
    QStringList privacyBrowserServiceNames = registeredServiceNames.filter(QLatin1String("com.stoutner.privacybrowser"));

    // Reapply domain settings in each window.
    for (QString privacyBrowserServiceName : privacyBrowserServiceNames)
    {
        // Prepare the D-Bus message.
        QDBusMessage dBusMessage = QDBusMessage::createMethodCall(privacyBrowserServiceName, QLatin1String("/privacybrowser/MainWindow_1"), QLatin1String("com.stoutner.privacybrowser.BrowserWindow"),
                                                                  methodString);

        // Make it so.
        QDBusConnection::sessionBus().send(dBusMessage);
    }
}

void BrowserWindow::showBookmarkContextMenu(const QPoint &point)
{
    // Get the bookmark action.
    QAction *bookmarkActionPointer = bookmarksToolBarPointer->actionAt(point);

    // Check to see if an action was clicked.
    if (bookmarkActionPointer)  // An action was clicked.
    {
        // Create a bookmark context menu.
        QMenu *bookmarkContextMenuPointer = new QMenu();

        // Get the database ID from the action.
        int databaseId = bookmarkActionPointer->data().toInt();

        // Create the menu according to the type.
        if (BookmarksDatabase::isFolder(databaseId))  // A folder was clicked.
        {
            // Populate the final bookmarks menu entries.  `false` does not add the actions to the delete list, as they will be automatically deleted when the menu closes.
            addBookmarkFolderFinalActions(bookmarkContextMenuPointer, BookmarksDatabase::getFolderId(databaseId), false);
        }
        else  // A bookmark was clicked.
        {
            // Add the open in new tab action to the menu.
            bookmarkContextMenuPointer->addAction(QIcon::fromTheme(QLatin1String("tab-new")), i18nc("Open bookmark in new tab context menu entry", "Open in New Tab"), [=]
                {
                    // Get the bookmark.
                    BookmarkStruct *bookmarkStructPointer = BookmarksDatabase::getBookmark(databaseId);

                    // Open the bookmark in a new tab.  `true` removes the URL line edit focus, `true` opens the new tab in an adjacent tab.  `false` does not load a background tab.
                    tabWidgetPointer->addTab(true, true, false, bookmarkStructPointer->url);
                }
            );

            // Add the open in background tab action to the menu.
            bookmarkContextMenuPointer->addAction(QIcon::fromTheme(QLatin1String("tab-new")), i18nc("Open bookmark in background tab context menu entry", "Open in Background Tab"), [=]
                {
                    // Get the bookmark.
                    BookmarkStruct *bookmarkStructPointer = BookmarksDatabase::getBookmark(databaseId);

                    // Open the bookmark in a new tab.  `true` removes the URL line edit focus, `true` opens the new tab in an adjacent tab.  `true` loads a background tab.
                    tabWidgetPointer->addTab(true, true, true, bookmarkStructPointer->url);
                }
            );

            // Add the open in new window action to the menu.
            bookmarkContextMenuPointer->addAction(QIcon::fromTheme(QLatin1String("window-new")), i18nc("Open bookmark in new window context menu entry", "Open in New Window"), [=]
                {
                    // Get the bookmark.
                    BookmarkStruct *bookmarkStructPointer = BookmarksDatabase::getBookmark(databaseId);

                    // Create a new browser window and load the first URL.  `false` indicates it is not the first browser window.
                    BrowserWindow *browserWindowPointer = new BrowserWindow(false, &bookmarkStructPointer->url);

                    // Show the new browser window.
                    browserWindowPointer->show();
                }
            );

            // Add a separator.
            bookmarkContextMenuPointer->addSeparator();

            // Add the edit action to the menu.
            bookmarkContextMenuPointer->addAction(QIcon::fromTheme(QLatin1String("document-edit")), i18nc("Edit bookmark context menu entry", "Edit"), [=]
                {
                    // Get the current tab favorite icon.
                    QIcon currentTabFavoriteIcon = tabWidgetPointer->getCurrentTabFavoritIcon();

                    // Instantiate an edit bookmark dialog.
                    QDialog *editBookmarkDialogPointer = new EditBookmarkDialog(this, databaseId, currentTabFavoriteIcon);

                    // Show the dialog.
                    editBookmarkDialogPointer->show();

                    // Process bookmark events.
                    connect(editBookmarkDialogPointer, SIGNAL(bookmarkSaved()), this, SLOT(populateBookmarksInAllWindows()));
                }
            );

            // Add the copy URL action to the menu.
            bookmarkContextMenuPointer->addAction(QIcon::fromTheme(QLatin1String("edit-copy")), i18nc("Copy bookmark URL context menu entry", "Copy URL"), [=]
                {
                    // Get the bookmark.
                    BookmarkStruct *bookmarkStructPointer = BookmarksDatabase::getBookmark(databaseId);

                    // Get a handle for the clipboard.
                    QClipboard *clipboard = QGuiApplication::clipboard();

                    // Place the URL on the keyboard.
                    clipboard->setText(bookmarkStructPointer->url);
                }
            );

            // Add a separator.
            bookmarkContextMenuPointer->addSeparator();

            // Add the delete action to the menu.
            bookmarkContextMenuPointer->addAction(QIcon::fromTheme(QLatin1String("edit-delete")), i18nc("Delete bookmark context menu entry", "Delete"), [=]
                {
                    // Get the parent folder ID.
                    double parentFolderId = BookmarksDatabase::getParentFolderId(databaseId);

                    // Delete the bookmark.
                    BookmarksDatabase::deleteBookmark(databaseId);

                    // Update the display order of the bookmarks in the parent folder.
                    BookmarksDatabase::updateFolderContentsDisplayOrder(parentFolderId);

                    // Repopulate the bookmarks.
                    populateBookmarksInAllWindows();
                }
            );
        }

        // Delete the menu from memory when it is closed.
        bookmarkContextMenuPointer->setAttribute(Qt::WA_DeleteOnClose);

        // Display the context menu.
        bookmarkContextMenuPointer->popup(bookmarksToolBarPointer->mapToGlobal(point));
    }
    else  // The toolbar background was clicked.
    {
        // Temporarily set the context menu policy to the default.
        bookmarksToolBarPointer->setContextMenuPolicy(Qt::DefaultContextMenu);

        // Create a context menu event with the same position.  The global position is set to the current mouse position.
        QContextMenuEvent *contextMenuEventPointer = new QContextMenuEvent(QContextMenuEvent::Mouse, point, QCursor::pos());

        // Send the context menu event to the toolbar.
        QCoreApplication::sendEvent(bookmarksToolBarPointer, contextMenuEventPointer);

        // Reset the context menu policy.
        bookmarksToolBarPointer->setContextMenuPolicy(Qt::CustomContextMenu);
    }
}

void BrowserWindow::showCookiesDialog()
{
    // Remove the focus from the URL line edit.
    urlLineEditPointer->clearFocus();

    // Instantiate the cookie settings dialog.
    CookiesDialog *cookiesDialogPointer = new CookiesDialog(tabWidgetPointer->getCookieList());

    // Show the dialog.
    cookiesDialogPointer->show();

    // Connect the dialog signals.
    connect(cookiesDialogPointer, SIGNAL(addCookie(QNetworkCookie)), tabWidgetPointer, SLOT(addCookieToStore(QNetworkCookie)));
    connect(cookiesDialogPointer, SIGNAL(deleteAllCookies()), tabWidgetPointer, SLOT(deleteAllCookies()));
    connect(cookiesDialogPointer, SIGNAL(deleteCookie(QNetworkCookie)), tabWidgetPointer, SLOT(deleteCookieFromStore(QNetworkCookie)));
}

void BrowserWindow::showDomainSettingsDialog()
{
    // Remove the focus from the URL line edit.
    urlLineEditPointer->clearFocus();

    // Instantiate the domain settings dialog.
    DomainSettingsDialog *domainSettingsDialogPointer = new DomainSettingsDialog(this);

    // Reload the tabs when domain settings are updated.
    connect(domainSettingsDialogPointer, SIGNAL(domainSettingsUpdated()), this, SLOT(applyDomainSettingsInAllWindows()));

    // Show the dialog.
    domainSettingsDialogPointer->show();
}

void BrowserWindow::showFilterListsDialog()
{
    // Instantiate the filter lists dialog.
    FilterListsDialog *filterListsDialogPointer = new FilterListsDialog(this);

    // Show the dialog.
    filterListsDialogPointer->show();
}

void BrowserWindow::showFindTextActions() const
{
    // Show the find text actions.
    findTextLineEditActionPointer->setVisible(true);
    findTextLabelActionPointer->setVisible(true);
    findNextActionPointer->setVisible(true);
    findPreviousActionPointer->setVisible(true);
    findCaseSensitiveActionPointer->setVisible(true);
    hideFindTextActionPointer->setVisible(true);

    // Set the focus on the find line edit.
    findTextLineEditPointer->setFocus();

    // Select all the text in the find line edit.
    findTextLineEditPointer->selectAll();
}

void BrowserWindow::showProgressBar(const int &progress) const
{
    // Set the progress bar value.
    progressBarPointer->setValue(progress);

    // Show the progress bar.
    progressBarPointer->show();

    // Disable and hide the refresh action.
    refreshActionPointer->setEnabled(false);
    refreshActionPointer->setVisible(false);

    // Enable and show the stop action.
    stopActionPointer->setEnabled(true);
    stopActionPointer->setVisible(true);
}

void BrowserWindow::showRequestsDialog()
{
    // Instantiate the requests dialog.
    RequestsDialog *requestsDialogPointer = new RequestsDialog(this, tabWidgetPointer->currentPrivacyWebEngineViewPointer->requestsListPointer);

    // Show the dialog.
    requestsDialogPointer->show();
}

void BrowserWindow::showSettingsDialog()
{
    // Get a handle for the KConfig skeleton.
    KConfigSkeleton *kConfigSkeletonPointer = Settings::self();

    // Instantiate a settings dialog.
    SettingsDialog *settingsDialogPointer = new SettingsDialog(this, kConfigSkeletonPointer);

    // Show the dialog
    settingsDialogPointer->show();

    // TODO.  KConfigDialog does not respect expanding size policies.  <https://redmine.stoutner.com/issues/823>
    //settingsDialogPointer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    //privacySettingsWidgetPointer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    //generalSettingsWidgetPointer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    //settingsDialogPointer->adjustSize();

    // Expand the config dialog.
    settingsDialogPointer->resize(1000, 500);

    // Apply the settings handled by KConfig.
    connect(settingsDialogPointer, SIGNAL(spellCheckLanguagesUpdated()), tabWidgetPointer, SLOT(applySpellCheckLanguages()));
    connect(settingsDialogPointer, SIGNAL(settingsChanged(QString)), tabWidgetPointer, SLOT(applyApplicationSettings()));
    //connect(settingsDialogPointer, SIGNAL(settingsChanged(QString)), this, SLOT(applyDomainSettingsInAllWindows()));
    connect(settingsDialogPointer, SIGNAL(settingsChanged(QString)), tabWidgetPointer, SLOT(applyDomainSettingsAndReload()));
}

QSize BrowserWindow::sizeHint() const
{
    // Return the default window size.
    return QSize(1500, 1200);
}

void BrowserWindow::toggleBookmark()
{
    // Remove the focus from the URL line edit, which will have been focused when the user clicked on the bookmarked icon.
    urlLineEditPointer->clearFocus();

    // Create or delete the bookmark
    if (bookmarkedActionPointer->isChecked())  // The user checked the toggle.  Create a bookmark.
    {
        // Create a bookmark struct.
        BookmarkStruct *bookmarkStructPointer = new BookmarkStruct;

        // Populate the bookmark struct.
        bookmarkStructPointer->name = tabWidgetPointer->getCurrentTabTitle();
        bookmarkStructPointer->url = tabWidgetPointer->getCurrentTabUrl();
        bookmarkStructPointer->parentFolderId = 0;
        bookmarkStructPointer->favoriteIcon = tabWidgetPointer->getCurrentTabFavoritIcon();

        // Add the bookmark.
        BookmarksDatabase::addBookmark(bookmarkStructPointer);
    }
    else  // The user unchecked the toggle.  Delete all related bookmarks.
    {
        // Delete the bookmarks.
        BookmarksDatabase::deleteBookmarks(urlLineEditPointer->text());


    }

    // Repopulate the bookmarks.
    populateBookmarksInAllWindows();
}

void BrowserWindow::toggleDeveloperTools() const
{
    // Toggle the developer tools.
    tabWidgetPointer->toggleDeveloperTools(developerToolsActionPointer->isChecked());
}

void BrowserWindow::toggleDomStorage() const
{
    // Remove the focus from the URL line edit.
    urlLineEditPointer->clearFocus();

    // Toggle DOM storage.
    tabWidgetPointer->toggleDomStorage();
}

void BrowserWindow::toggleFindCaseSensitive() const
{
    // Get the current find string.
    const QString findString = findTextLineEditPointer->text();

    // Toggle find case sensitive.
    tabWidgetPointer->toggleFindCaseSensitive(findString);
}

void BrowserWindow::toggleJavaScript() const
{
    // Remove the focus from the URL line edit.
    urlLineEditPointer->clearFocus();

    // Toggle JavaScript.
    tabWidgetPointer->toggleJavaScript();
}

void BrowserWindow::toggleLocalStorage() const
{
    // Remove the focus from the URL line edit.
    urlLineEditPointer->clearFocus();

    // Toggle local storage.
    tabWidgetPointer->toggleLocalStorage();
}

void BrowserWindow::toggleFullScreen()
{
    // Toggle the full screen status.
    fullScreenRequested(fullScreenActionPointer->isChecked());
}

void BrowserWindow::toggleViewSource() const
{
    // Get the current URL.
    QString url = urlLineEditPointer->text();

    // Toggle the URL.
    if (url.startsWith(QLatin1String("view-source:")))  // The source is currently being viewed.
    {
        // Remove `view-source:` from the URL.
        url = url.remove(0, 12);
    }
    else  // The source is not currently being viewed.
    {
        // Prepend `view-source:` from the URL.
        url = url.prepend(QLatin1String("view-source:"));
    }

    // Make it so.
    loadUrlFromLineEdit(url);
}

void BrowserWindow::toggleViewBookmarksToolBar()
{
    // Store the current status of the bookmarks toolbar, which is used when exiting full screen browsing.
    bookmarksToolBarIsVisible = viewBookmarksToolBarActionPointer->isChecked();

    // Update the visibility of the bookmarks toolbar.
    bookmarksToolBarPointer->setVisible(bookmarksToolBarIsVisible);
}

void BrowserWindow::toggleViewSourceInNewTab() const
{
    // Get the current URL.
    QString urlString = urlLineEditPointer->text();

    // Toggle the URL.
    if (urlString.startsWith(QLatin1String("view-source:")))  // The source is currently being viewed.
    {
        // Remove `view-source:` from the URL.
        urlString = urlString.remove(0, 12);
    }
    else  // The source is not currently being viewed.
    {
        // Prepend `view-source:` from the URL.
        urlString = urlString.prepend(QLatin1String("view-source:"));
    }

    // Add the new tab.  `true` removes the URL line edit focus, `true` opens the new tab in an adjacent tab.  `false` does not open a background tab.
    tabWidgetPointer->addTab(true, true, false, urlString);
}

void BrowserWindow::updateBookmarkedAction() const
{
    // Update the bookmarked action to reflect the current state.
    if (bookmarkedActionPointer->isChecked())
        bookmarkedActionPointer->setIcon(QIcon::fromTheme(QLatin1String("starred-symbolic")));
    else
        bookmarkedActionPointer->setIcon(QIcon::fromTheme(QLatin1String("non-starred-symbolic")));
}

void BrowserWindow::updateRequestsAction(const QVector<int> blockedRequestsVector) const
{
    // Update the text for the actions.
    ultraPrivacyActionPointer->setText(i18nc("UltraPrivacy action", "UltraPrivacy - %1 Blocked", blockedRequestsVector[PrivacyWebEngineView::ULTRAPRIVACY]));
    ultraListActionPointer->setText(i18nc("UltraList action", "UltraList - %1 Blocked", blockedRequestsVector[PrivacyWebEngineView::ULTRALIST]));
    easyPrivacyActionPointer->setText(i18nc("EasyPrivacy action", "EasyPrivacy - %1 Blocked", blockedRequestsVector[PrivacyWebEngineView::EASYPRIVACY]));
    easyListActionPointer->setText(i18nc("EasyList action", "EasyList - %1 Blocked", blockedRequestsVector[PrivacyWebEngineView::EASYLIST]));
    fanboysAnnoyanceListPointer->setText(i18nc("Fanboy's Annoyance List action", "Fanboy’s Annoyance List - %1 Blocked", blockedRequestsVector[PrivacyWebEngineView::FANBOYS_ANNOYANCE_LIST]));
    requestsActionPointer->setText(i18nc("Requests action", "Requests - %1 Blocked", blockedRequestsVector[PrivacyWebEngineView::TOTAL]));
    blockedRequestsActionPointer->setText(QString::number(blockedRequestsVector[PrivacyWebEngineView::TOTAL]));

    // Update the tool tips.
    blockedRequestsActionPointer->setToolTip(i18nc("Blocked requests action tool tip", "%1 Blocked Requests (Ctrl+R)", blockedRequestsVector[PrivacyWebEngineView::TOTAL]));

    // Update the blocked requests action background color.
    if (blockedRequestsVector[PrivacyWebEngineView::TOTAL] > 0)
        urlToolBarPointer->setStyleSheet(QLatin1String("QToolButton#blocked_requests { background-color: tomato; border-radius: 5px; padding: 4px; }"));
    else
        urlToolBarPointer->setStyleSheet(QLatin1String("QToolButton#blocked_requests { background-color: transparent; }"));

    // Restore the domain settings indicator.
    if (domainSettingsApplied)
        urlLineEditPointer->setPalette(positiveBackgroundPalette);
    else
        urlLineEditPointer->setPalette(normalBackgroundPalette);
}

void BrowserWindow::updateCookiesAction(const int numberOfCookies) const
{
    // Update the cookies action text.
    cookiesActionPointer->setText(i18nc("The Cookies action, which also displays the number of cookies", "Cookies - %1", numberOfCookies));
}

void BrowserWindow::updateDefaultZoomFactor(const double newDefaultZoomFactorDouble)
{
    // Store the new default zoom factor.
    defaultZoomFactorDouble = newDefaultZoomFactorDouble;
}

void BrowserWindow::updateDomStorageAction(const bool &isEnabled) const
{
    // Set the action checked status.
    domStorageActionPointer->setChecked(isEnabled);
}

void BrowserWindow::updateDomainSettingsIndicator(const bool status)
{
    // Store the domain settings applied status.
    domainSettingsApplied = status;

    // Set the domain palette according to the status.
    if (domainSettingsApplied)
        urlLineEditPointer->setPalette(positiveBackgroundPalette);
    else
        urlLineEditPointer->setPalette(normalBackgroundPalette);
}

void BrowserWindow::updateFindText(const QString &text, const bool findCaseSensitive) const
{
    // Set the text.
    findTextLineEditPointer->setText(text);

    // Set the find case sensitive action checked status.
    findCaseSensitiveActionPointer->setChecked(findCaseSensitive);
}

void BrowserWindow::updateFindTextResults(const QWebEngineFindTextResult &findTextResult) const
{
    // Update the find text label.
    findTextLabelPointer->setText(QStringLiteral("  %1/%2  ").arg(findTextResult.activeMatch()).arg(findTextResult.numberOfMatches()));

    // Set the background color according to the find status.
    if (findTextLineEditPointer->text().isEmpty())
        findTextLineEditPointer->setPalette(normalBackgroundPalette);
    else if (findTextResult.numberOfMatches() == 0)
        findTextLineEditPointer->setPalette(negativeBackgroundPalette);
    else
        findTextLineEditPointer->setPalette(positiveBackgroundPalette);
}

void BrowserWindow::updateJavaScriptAction(const bool &isEnabled)
{
    // Update the JavaScript status.
    javaScriptEnabled = isEnabled;

    // Set the icon according to the status.
    if (javaScriptEnabled)
        javaScriptActionPointer->setIcon(QIcon(QLatin1String(":/icons/javascript-warning.svg")));
    else
        javaScriptActionPointer->setIcon(QIcon(QLatin1String(":/icons/privacy-mode.svg")));

    // Set the action checked status.
    javaScriptActionPointer->setChecked(javaScriptEnabled);

    // Update the status of the DOM storage action.
    domStorageActionPointer->setEnabled(javaScriptEnabled & localStorageEnabled);
}

void BrowserWindow::updateLocalStorageAction(const bool &isEnabled)
{
    // Update the local storage status.
    localStorageEnabled = isEnabled;

    // Update the icon.  On Gnome, the toolbar icons don't pick up unless the size is explicit, probably because the toolbar ends up being an intermediate size.
    if (localStorageEnabled)
        localStorageActionPointer->setIcon(QIcon::fromTheme(QLatin1String("disk-quota-high"), QIcon(QLatin1String("/usr/share/icons/gnome/32x32/actions/document-save-as.png"))));
    else
        localStorageActionPointer->setIcon(QIcon::fromTheme(QLatin1String("disk-quota"), QIcon(QLatin1String("/usr/share/icons/gnome/32x32/apps/kfm.png"))));

    // Set the action checked status.
    localStorageActionPointer->setChecked(localStorageEnabled);

    // Update the status of the DOM storage action.
    domStorageActionPointer->setEnabled(localStorageEnabled & javaScriptEnabled);
}

void BrowserWindow::updateSearchEngineActions(const QString &searchEngine, const bool &updateCustomSearchEngineStatus)
{
    // Initialize the custom search engine flag.
    bool customSearchEngine = false;

    if (searchEngine == QLatin1String("Mojeek"))  // Mojeek.
    {
        // Check the Mojeek user agent action.
        searchEngineMojeekActionPointer->setChecked(true);

        // Update the search engine menu action icon.
        searchEngineMenuActionPointer->setIcon(QIcon::fromTheme(QLatin1String("edit-find")));

        // Update the search engine menu action text.
        searchEngineMenuActionPointer->setText(i18nc("The main search engine menu action", "Search Engine - Mojeek"));
    }
    else if (searchEngine == QLatin1String("Monocles"))  // Monocles.
    {
        // Check the Monocles user agent action.
        searchEngineMonoclesActionPointer->setChecked(true);

        // Update the search engine menu action icon.
        searchEngineMenuActionPointer->setIcon(QIcon::fromTheme(QLatin1String("edit-find")));

        // Update the search engine menu action text.
        searchEngineMenuActionPointer->setText(i18nc("The main search engine menu action", "Search Engine - Monocles"));
    }
    else if (searchEngine == QLatin1String("MetaGer"))  // MetaGer.
    {
        // Check the MetaGer user agent action.
        searchEngineMetagerActionPointer->setChecked(true);

        // Update the search engine menu action icon.
        searchEngineMenuActionPointer->setIcon(QIcon::fromTheme(QLatin1String("edit-find")));

        // Update the search engine menu action text.
        searchEngineMenuActionPointer->setText(i18nc("The main search engine menu action", "Search Engine - MetaGer"));
    }
    else if (searchEngine == QLatin1String("Google"))  // Google.
    {
        // Check the Google user agent action.
        searchEngineGoogleActionPointer->setChecked(true);

        // Update the search engine menu action icon.
        searchEngineMenuActionPointer->setIcon(QIcon::fromTheme(QLatin1String("im-google"), QIcon::fromTheme(QLatin1String("edit-find"))));

        // Update the search engine menu action text.
        searchEngineMenuActionPointer->setText(i18nc("The main search engine menu action", "Search Engine - Google"));
    }
    else if (searchEngine == QLatin1String("Bing"))  // Bing.
    {
        // Check the Bing user agent action.
        searchEngineBingActionPointer->setChecked(true);

        // Update the search engine menu action icon.
        searchEngineMenuActionPointer->setIcon(QIcon::fromTheme(QLatin1String("edit-find")));

        // Update the search engine menu action text.
        searchEngineMenuActionPointer->setText(i18nc("The main search engine menu action", "Search Engine - Bing"));
    }
    else if (searchEngine == QLatin1String("Yahoo"))  // Yahoo.
    {
        // Check the Yahoo user agent action.
        searchEngineYahooActionPointer->setChecked(true);

        // Update the search engine menu action icon.
        searchEngineMenuActionPointer->setIcon(QIcon::fromTheme(QLatin1String("im-yahoo"), QIcon::fromTheme(QLatin1String("edit-find"))));

        // Update the search engine menu action text.
        searchEngineMenuActionPointer->setText(i18nc("The main search engine menu action", "Search Engine - Yahoo"));
    }
    else  // Custom search engine.
    {
        // Check the user agent.
        searchEngineCustomActionPointer->setChecked(true);

        // Update the search engine menu action icon.
        searchEngineMenuActionPointer->setIcon(QIcon::fromTheme(QLatin1String("edit-find")));

        // Update the search engine menu action text.
        searchEngineMenuActionPointer->setText(i18nc("The main search engine menu action", "Search Engine - Custom"));

        // Set the custom search engine text.
        searchEngineCustomActionPointer->setText(searchEngine);

        // Set the custom search engine flag.
        customSearchEngine = true;
    }

    // Update the custom search engine enabled boolean.
    if (updateCustomSearchEngineStatus)
        customSearchEngineEnabled = customSearchEngine;

    // Format the custom search engine.
    if (customSearchEngineEnabled)
    {
        // Enable the custom search engine.
        searchEngineCustomActionPointer->setEnabled(true);
    }
    else
    {
        // Disable the custom search engine.
        searchEngineCustomActionPointer->setEnabled(false);

        // Reset the custom search engine text.
        searchEngineCustomActionPointer->setText(i18nc("@action", "Custom"));
    }
}

void BrowserWindow::updateUrlLineEdit(const QUrl &newUrl)
{
    // Get the new URL string in encoded form, which displays punycode.
    QString newUrlString = QLatin1String(newUrl.toEncoded());

    // Update the view source actions.
    if (newUrlString.startsWith(QLatin1String("view-source:")))  // The source is currently being viewed.
    {
        // Mark the view source checkbox.
        viewSourceActionPointer->setChecked(true);

        // Update the view in new tab action text.
        viewSourceInNewTabActionPointer->setText(i18nc("View rendered website in new tab action", "View Rendered Website in New Tab"));
    }
    else  // The source is not currently being viewed.
    {
        // Unmark the view source checkbox.
        viewSourceActionPointer->setChecked(false);

        // Update the view in new tab action text.
        viewSourceInNewTabActionPointer->setText(i18nc("View source in new tab action", "View Source in New Tab"));
    }

    // Update the URL line edit if it does not have focus.
    if (!urlLineEditPointer->hasFocus())
    {
        // Update the URL line edit.
        urlLineEditPointer->setText(newUrlString);

        // Set the bookmarked action status.
        bookmarkedActionPointer->setChecked(BookmarksDatabase::isBookmarked(newUrlString));

        // Update the bookmarked action.
        updateBookmarkedAction();

        // Set the focus if the new URL is blank.
        if (newUrlString == QStringLiteral(""))
            urlLineEditPointer->setFocus();
    }

    // Store the current URL.
    currentUrl = newUrl;
}

void BrowserWindow::updateUserAgentActions(const QString &userAgent, const bool &updateCustomUserAgentStatus)
{
    // Initialize the custom user agent flag.
    bool customUserAgent = false;

    // Check the indicated on-the-fly user agent.
    if (userAgent == UserAgentHelper::PRIVACY_BROWSER_USER_AGENT)  // Privacy Browser.
    {
        // Check the Privacy Browser user agent action.
        userAgentPrivacyBrowserActionPointer->setChecked(true);

        // Update the user agent menu action icon.
        userAgentMenuActionPointer->setIcon(QIcon(QLatin1String(":/icons/privacy-mode.svg")));

        // Update the user agent menu action text.
        userAgentMenuActionPointer->setText(i18nc("The main user agent menu action", "User Agent - Privacy Browser"));
    }
    else if (userAgent == TabWidget::webEngineDefaultUserAgent)  // WebEngine default.
    {
        // check the WebEngine default user agent action.
        userAgentWebEngineDefaultActionPointer->setChecked(true);

        // Update the user agent menu action icon.
        userAgentMenuActionPointer->setIcon(QIcon::fromTheme(QLatin1String("qtlogo"), QIcon::fromTheme(QLatin1String("user-group-properties"), QIcon::fromTheme(QLatin1String("contact-new")))));

        // Update the user agent menu action text.
        userAgentMenuActionPointer->setText(i18nc("The main user agent menu action", "User Agent - WebEngine default"));
    }
    else if (userAgent == UserAgentHelper::FIREFOX_LINUX_USER_AGENT)  // Firefox on Linux.
    {
        // Check the Firefox on Linux user agent action.
        userAgentFirefoxLinuxActionPointer->setChecked(true);

        // Update the user agent menu action icon.
        userAgentMenuActionPointer->setIcon(QIcon::fromTheme(QLatin1String("firefox-esr"), QIcon::fromTheme(QLatin1String("user-group-properties"), QIcon::fromTheme(QLatin1String("contact-new")))));

        // Update the user agent menu action text.
        userAgentMenuActionPointer->setText(i18nc("The main user agent menu action", "User Agent - Firefox on Linux"));
    }
    else if (userAgent == UserAgentHelper::CHROMIUM_LINUX_USER_AGENT)  // Chromium on Linux.
    {
        // Check the Chromium on Linux user agent action.
        userAgentChromiumLinuxActionPointer->setChecked(true);

        // Update the user agent menu action icon.
        userAgentMenuActionPointer->setIcon(QIcon::fromTheme(QLatin1String("chromium"), QIcon::fromTheme(QLatin1String("user-group-properties"), QIcon::fromTheme(QLatin1String("contact-new")))));

        // Update the user agent menu action text.
        userAgentMenuActionPointer->setText(i18nc("The main user agent menu action", "User Agent - Chromium on Linux"));
    }
    else if (userAgent == UserAgentHelper::FIREFOX_WINDOWS_USER_AGENT)  // Firefox on Windows.
    {
        // Check the Firefox on Windows user agent action.
        userAgentFirefoxWindowsActionPointer->setChecked(true);

        // Update the user agent menu action icon.
        userAgentMenuActionPointer->setIcon(QIcon::fromTheme(QLatin1String("firefox-esr"), QIcon::fromTheme(QLatin1String("user-group-properties"), QIcon::fromTheme(QLatin1String("contact-new")))));

        // Update the user agent menu action text.
        userAgentMenuActionPointer->setText(i18nc("The main user agent menu action", "User Agent - Firefox on Windows"));
    }
    else if (userAgent == UserAgentHelper::CHROME_WINDOWS_USER_AGENT)  // Chrome on Windows.
    {
        // Check the Chrome on Windows user agent action.
        userAgentChromeWindowsActionPointer->setChecked(true);

        // Update the user agent menu action icon.
        userAgentMenuActionPointer->setIcon(QIcon::fromTheme(QLatin1String("chromium"), QIcon::fromTheme(QLatin1String("user-group-properties"), QIcon::fromTheme(QLatin1String("contact-new")))));

        // Update the user agent menu action text.
        userAgentMenuActionPointer->setText(i18nc("The main user agent menu action", "User Agent - Chrome on Windows"));
    }
    else if (userAgent == UserAgentHelper::EDGE_WINDOWS_USER_AGENT)  // Edge on Windows.
    {
        // Check the Edge on Windows user agent action.
        userAgentEdgeWindowsActionPointer->setChecked(true);

        // Update the user agent menu action icon.
        userAgentMenuActionPointer->setIcon(QIcon::fromTheme(QLatin1String("user-group-properties"), QIcon::fromTheme(QLatin1String("contact-new"))));

        // Update the user agent menu action text.
        userAgentMenuActionPointer->setText(i18nc("The main user agent menu action", "User Agent - Edge on Windows"));
    }
    else if (userAgent == UserAgentHelper::SAFARI_MACOS_USER_AGENT)  // Safari on macOS.
    {
        // Check the Safari on macOS user agent action.
        userAgentSafariMacosActionPointer->setChecked(true);

        // Update the user agent menu action icon.
        userAgentMenuActionPointer->setIcon(QIcon::fromTheme(QLatin1String("user-group-properties"), QIcon::fromTheme(QLatin1String("contact-new"))));

        // Update the user agent menu action text.
        userAgentMenuActionPointer->setText(i18nc("The main user agent menu action", "User Agent - Safari on macOS"));
    }
    else  // Custom user agent.
    {
        // Check the user agent.
        userAgentCustomActionPointer->setChecked(true);

        // Update the user agent menu action icon.
        userAgentMenuActionPointer->setIcon(QIcon::fromTheme(QLatin1String("user-group-properties"), QIcon::fromTheme(QLatin1String("contact-new"))));

        // Update the user agent menu action text.
        userAgentMenuActionPointer->setText(i18nc("The main user agent menu action", "User Agent - Custom"));

        // Set the custom user agent text.
        userAgentCustomActionPointer->setText(userAgent);

        // Set the custom user agent flag.
        customUserAgent = true;
    }

    // Update the custom user agent enabled boolean.
    // This is not done when the current user agent is a custom one but it is temporarially being changed via on-the-fly settings so that the user can switch back to the custom user agent.
    if (updateCustomUserAgentStatus)
        customUserAgentEnabled = customUserAgent;


    // Format the custom user agent.
    if (customUserAgentEnabled)
    {
        // Enable the custom user agent.
        userAgentCustomActionPointer->setEnabled(true);
    }
    else
    {
        // Disable the custom user agent.
        userAgentCustomActionPointer->setEnabled(false);

        // Reset the custom user agent text.
        userAgentCustomActionPointer->setText(i18nc("@action", "Custom"));
    }
}

void BrowserWindow::updateViewBookmarksToolBarCheckbox(const bool visible)
{
    // Update the view bookmarks toolbar checkbox.
    viewBookmarksToolBarActionPointer->setChecked(visible);

    // Initialize the bookmarks toolbar visibility tracker if Privacy Browser has just launched.
    if (bookmarksToolBarUninitialized)
    {
        // Set the initialization flag.
        bookmarksToolBarUninitialized = false;

        // Store the current status of the bookmarks toolbar, which is used when exiting full screen browsing.
        bookmarksToolBarIsVisible = visible;
    }
}

void BrowserWindow::updateWindowTitle(const QString &title)
{
    // Update the window title.
    setWindowTitle(title);
}

void BrowserWindow::updateZoomActions(const double zoomFactorDouble)
{
    // Set the current zoom factor.
    currentZoomFactorDouble = zoomFactorDouble;

    // Set the status of the default zoom action.
    zoomDefaultActionPointer->setEnabled(currentZoomFactorDouble != defaultZoomFactorDouble);

    // Set the status of the zoom in action and button.
    zoomInActionPointer->setEnabled(currentZoomFactorDouble <= 4.99);
    zoomPlusButtonPointer->setEnabled(currentZoomFactorDouble <= 4.99);

    // Set the status of the zoom out action and button.
    zoomMinusButtonPointer->setEnabled(currentZoomFactorDouble > 0.25);
    zoomOutActionPointer->setEnabled(currentZoomFactorDouble > 0.25);


    // Update the zoom factor action text, formatting the double with 2 decimal places.  `0` specifies no extra field width.  `'0'` sets the format to not use scientific notation.
    zoomFactorActionPointer->setText(ki18nc("The zoom factor action", "Zoom Factor - %1").subs(zoomFactorDouble, 0, '0', 2).toString());

    // Update the status bar zoom factor label.
    currentZoomButtonPointer->setText(ki18nc("The status bar zoom, which is just the formatted zoom factor", "%1").subs(zoomFactorDouble, 0, '0', 2).toString());
}

void BrowserWindow::zoomDefault()
{
    // Set the new zoom factor.
    tabWidgetPointer->applyOnTheFlyZoomFactor(defaultZoomFactorDouble);

    // Update the on-the-fly action text.
    updateZoomActions(defaultZoomFactorDouble);
}
