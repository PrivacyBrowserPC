/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2023 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BOOKMARKSDATABASE_H
#define BOOKMARKSDATABASE_H

// Application headers.
#include "structs/BookmarkStruct.h"

// Qt framework headers.
#include <QtSql>

class BookmarksDatabase
{
public:
    // The default constructor.
    BookmarksDatabase();

    // The public functions.
    static void addBookmark(const BookmarkStruct *bookmarkStructPointer);
    static void addDatabase();
    static void addFolder(const BookmarkStruct *bookmarkStructPointer);
    static void deleteBookmark(const int databaseId);
    static void deleteBookmarks(const QString url);
    static QList<QString>* getAllFolderUrls(const double folderId);
    static BookmarkStruct* getBookmark(const int databaseId);
    static std::list<BookmarkStruct>* getBookmarks();
    static QList<BookmarkStruct>* getBookmarksInFolderExcept(const double folderId, QList<int> *exceptDatabaseIdsListPointer);
    static QList<int>* getFolderContentsDatabaseIds(const double folderId);
    static QList<int>* getFolderContentsDatabaseIdsRecursively(const double folderId);
    static QList<BookmarkStruct>* getFolderContents(const double folderId);
    static int getFolderDatabaseId(const double folderId);
    static double getFolderId(const int databaseId);
    static int getFolderItemCount(const double folderId);
    static double getParentFolderId(const int databaseId);
    static QList<BookmarkStruct>* getSubfolders(const double folderId);
    static bool isBookmarked(const QString url);
    static bool isFolder(const int databaseId);
    static void updateBookmark(const BookmarkStruct *bookmarkStructPointer);
    static void updateBookmarkName(const int databaseId, const QString &bookmarkName);
    static void updateBookmarkUrl(const int databaseId, const QString &bookmarkUrl);
    static void updateDisplayOrder(const int databaseId, const int displayOrder);
    static void updateFolderContentsDisplayOrder(const double folderId);
    static void updateParentFolderAndDisplayOrder(const int databaseId, const double parentFolderId, const int displayOrder);

    // The public constants.
    static const QString CONNECTION_NAME;
    static const QString BOOKMARK_NAME;
    static const QString BOOKMARKS_TABLE;
    static const QString BOOKMARK_URL;
    static const QString DISPLAY_ORDER;
    static const QString FAVORITE_ICON;
    static const QString FOLDER_ID;
    static const QString ID;
    static const QString IS_FOLDER;
    static const QString PARENT_FOLDER_ID;

private:
    // The private static constants.
    static const int SCHEMA_VERSION;

    // The private functions.
    static double generateFolderId();
    static QString getFavoriteIconBase64String(const QIcon &favoriteIcon);
};
#endif
