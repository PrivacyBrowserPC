/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022, 2024 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc/>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef USERAGENTHELPER_H
#define USERAGENTHELPER_H

// Qt framework headers.
#include <QString>

class UserAgentHelper
{
public:
    // The default constructor.
    UserAgentHelper();

    // The destructor.
    ~UserAgentHelper();

    // The public static constants.
    static const QString CHROMIUM_LINUX_DATABASE;
    static const QString CHROMIUM_LINUX_USER_AGENT;
    static const QString CHROME_WINDOWS_DATABASE;
    static const QString CHROME_WINDOWS_USER_AGENT;
    static const QString EDGE_WINDOWS_DATABASE;
    static const QString EDGE_WINDOWS_USER_AGENT;
    static const QString FIREFOX_LINUX_DATABASE;
    static const QString FIREFOX_LINUX_USER_AGENT;
    static const QString FIREFOX_WINDOWS_DATABASE;
    static const QString FIREFOX_WINDOWS_USER_AGENT;
    static const QString PRIVACY_BROWSER_DATABASE;
    static const QString PRIVACY_BROWSER_USER_AGENT;
    static const QString SAFARI_MACOS_DATABASE;
    static const QString SAFARI_MACOS_USER_AGENT;
    static const QString SYSTEM_DEFAULT_DATABASE;
    static const QString WEB_ENGINE_DEFAULT_DATABASE;

    // The public static functions.
    static QString getDatabaseUserAgentNameFromUserAgent(const QString &userAgent);
    static int getDomainSettingsUserAgentIndex(const QString &userAgentName);
    static QString getUserAgentFromDatabaseName(const QString &userAgentDatabaseName);
    static QString getResultingDomainSettingsUserAgent(const QString &rawUserAgent);

    // The public variables.
    QString SYSTEM_DEFAULT_TRANSLATED;
    QString PRIVACY_BROWSER_TRANSLATED;
    QString WEB_ENGINE_DEFAULT_TRANSLATED;
    QString FIREFOX_LINUX_TRANSLATED;
    QString CHROMIUM_LINUX_TRANSLATED;
    QString FIREFOX_WINDOWS_TRANSLATED;
    QString CHROME_WINDOWS_TRANSLATED;
    QString EDGE_WINDOWS_TRANSLATED;
    QString SAFARI_MACOS_TRANSLATED;

    // The public functions.  Anything that accesses the translated user names must use an instantiated copy of the class.
    QString getDatabaseUserAgentNameFromTranslatedName(const QString &translatedUserAgentName);
    QString getUserAgentFromTranslatedName(const QString &userAgentTranslatedName);
    QString getTranslatedUserAgentNameFromDatabaseName(const QString &userAgentName);
};
#endif
