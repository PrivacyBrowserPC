/*
 * Copyright 2023 Soren Stoutner <soren@stoutner.com>.
 *
 * This file is part of Privacy Browser PC <https://www.stoutner.com/privacy-browser-pc>.
 *
 * Privacy Browser PC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Privacy Browser PC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Privacy Browser PC.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FOLDERHELPER_H
#define FOLDERHELPER_H

// Qt toolkit headers.
#include <QTreeWidgetItem>

class FolderHelper
{
public:
    // The default constructor.
    explicit FolderHelper();

    // The public constants.
    const int FOLDER_NAME_COLUMN = 0;
    const int FOLDER_ID_COLUMN = 1;

    // The public functions.
    void populateSubfolders(QTreeWidgetItem *treeWidgetItemPointer, const double initialParentFolderId);
    void populateSubfoldersExcept(const double exceptSubfolderDatabaseId, QTreeWidgetItem *treeWidgetItemPointer, const double initialParentFolderId);
};
#endif
